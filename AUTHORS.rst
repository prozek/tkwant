=================
Authors of tkwant
=================

The principal developers of tkwant are

* Joseph Weston (INAC/CEA Grenoble)
* Xavier Waintal (INAC/CEA Grenoble)
* Christoph Groth (INAC/CEA Grenoble)

(CEA = Commissariat à l'énergie atomique et aux énergies alternatives)
