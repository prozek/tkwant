# -*- coding: utf-8 -*-
# Copyright 2016 tkwant authors.
#
# This file is part of tkwant.  It is subject to the license terms in the file
# LICENSE.rst found in the top-level directory of this distribution and at
# http://kwant-project.org/license.  A list of tkwant authors can be found in
# the file AUTHORS.rst at the top-level directory of this distribution and at
# http://kwant-project.org/authors.
"""Tools for calculating integrals adaptatively and asynchronously."""

import abc
import bisect
import asyncio
import numpy as np


class Interval(metaclass=abc.ABCMeta):
    """Abstract base class for a single integration interval.

    Attributes
    ----------
    ab : pair of float
        The left and right limits of the integration interval.
    x : array of float
        The abscissae in this interval at which the integrand
        is evaluated.
    y : array of float
        The evaluated integrand. May be a 2D array if the integrand
        is vector valued.
    """
    __slots__ = ('ab', 'y', 'future')

    def __init__(self, a, b, y):
        self.ab = a, b
        self.y = y
        self.future = None

    @property
    def x(self):
        return self.xs_from_points(self.ab)

    @abc.abstractstaticmethod
    def xs_from_points(points):
        """Get the xs associated with a set of start/end points."""

    @abc.abstractstaticmethod
    def from_points(points, ys):
        """Create intervals from a set of start/end points."""

    @abc.abstractmethod
    @asyncio.coroutine
    def split(self):
        """Split the interval."""

    @abc.abstractproperty
    def err(self):
        """The absolute error of the integral over this interval."""

    @abc.abstractproperty
    def val(self):
        """The value of the integral over this interval."""

    @abc.abstractproperty
    def new_xs(self):
        """The extra points in the intervals after splitting."""

    def submit(self, ffactory, priority):
        """Submit extra points for evaluation in preparation for splitting."""
        if self.future:
            return
        self.future = ffactory(priority, self.new_xs)

    def __lt__(self, other):
        return self.err < other.err

    def __eq__(self, other):
        return self.err == other.err


def intervals_to_integrand(intervals):
    """Return integrand values from a sequence of intervals.

    Parameters
    ----------
    intervals : sequence of `~tkwant.integration.Interval`

    Returns
    -------
    ``(xs, ys)``
        The abscissae and integrand values for all the intervals.
    """
    # ensure the intervals are sorted by their position along the x axis.
    sorted_ivals = list(sorted(intervals, key=lambda ival: ival.ab))
    xs = np.concatenate([ival.x for ival in sorted_ivals])
    ys = np.concatenate([ival.y for ival in sorted_ivals])
    return xs, ys


_minus_inf = float("-inf")


@asyncio.coroutine
def adaptive_integration(interval_type, ffactory, a, b,
                         atol=1e-6, extra=0, limit=100, sness=1,
                         return_intervals=False):
    """Coroutine for asynchronous adaptive integration.

    Parameters
    ----------
    interval_type : Interval
        The type of interval to use for evaluating individual intervals.
    ffactory : callable
        Called with (priority, xs) and returns a future that evaluates the
        integrand at the points xs.
    a, b : int
        The limits of integration.
    atol : float, default: 1E-6
        The absolute tolerance on the integral.
    extra : iterable of floats OR int, default: 0
        Either: extra points to be inserted between a and b.
        Or: number of regularly spaced extra points to be inserted.
    limit : int, default: 100
        The maximum number of intervals to create before raising an error.
        If -1, then the algorithm will continue subdividing until the desired
        accuracy is reached.
    sness : float, default: 1
        Degree for speculativeness
    return_intervals : bool, default: False
        When True, return the final set of intervals used to evaluate the
        integral, in addition to the result and error. The intervals are
        sorted by position
    """
    invert = a > b
    if invert:
        a, b = b, a

    points = [a]
    try:
        # Try if "extra" is an iterable.
        points.extend(extra)
    except TypeError:
        # Treat "extra" as an integer.
        if int(extra) != extra:
            raise ValueError("The number of extra points must be an integer.")

        points = np.linspace(a, b, extra + 2)
    else:
        points.append(b)
        points.sort()
        if points[0] != a or points[-1] != b:
            raise ValueError("Extra points must lie between a and b.")

    xs = interval_type.xs_from_points(points)
    ys = yield from asyncio.wait_for(ffactory(_minus_inf, xs), None)
    ivals = interval_type.from_points(points, np.array(ys))
    ivals.sort()

    due_err = sum(ival.err for ival in ivals) - atol

    # Main loop.
    while due_err > 0:
        # Launch future for the worst interval.
        worst = ivals.pop()
        worst.submit(ffactory, _minus_inf)

        # Launch futures for other bad intervals.
        last_err = submitted_err = worst.err
        for ival in reversed(ivals):
            if submitted_err < due_err:
                last_err = ival.err
                submitted_err += last_err
            elif ival.err < last_err / sness:
                break
            ival.submit(ffactory, -ival.err / atol)

        new_ivals = yield from worst.split()
        due_err += sum(iv.err for iv in new_ivals) - worst.err
        for new in new_ivals:
            bisect.insort(ivals, new)
        if len(ivals) > limit and limit != -1:
            raise RuntimeError(
                "Desired accuracy was not achieved within the interval limit.")

        assert worst.future.done()

    result = 0
    n_wasted = 0
    for ival in ivals:
        if ival.future:
            n_wasted += 1
            ival.future.cancel()
        result += ival.val

    if invert:
        result = -result
        for ival in ivals:
            ival.ab = tuple(reversed(ival.ab))

    if return_intervals:
        return result, due_err + atol, list(sorted(ivals, key=lambda iv: iv.ab))
    else:
        return result, due_err + atol


######## Specific adaptive integrators

# Simpson's rules for whole interval, left & right half.
_sa, _sl, _sr = np.array([[1.0, 0.0, 4.0, 0.0, 1.0],
                          [0.5, 2.0, 0.5, 0.0, 0.0],
                          [0.0, 0.0, 0.5, 2.0, 0.5]]) / 6

# Richardson extrapolation
_simps_err_rule = (_sl + _sr - _sa) / 15
_simps_val_rule = (_sl + _sr) + _simps_err_rule

_simps_inter_points = np.array([1/8, 3/8, 5/8, 7/8])


class SimpsonInterval(Interval):
    """Integral evaluated using a 3(5)-point Simpson's rule."""

    @staticmethod
    def xs_from_points(points):
        """Get the xs associated with a set of start/end points."""
        # Add three equally spaced points between every two original points.
        xs = np.empty(len(points) * 4 - 3, float)
        xs[::4] = points
        xs[2::4] = (xs[:-1:4] + xs[4::4]) / 2
        xs[1::2] = (xs[0:-1:2] + xs[2::2]) / 2
        return xs

    @property
    def new_xs(self):
        """The new points in the intervals after splitting."""
        a, b = self.ab
        return _simps_inter_points * (b - a) + a

    @staticmethod
    def from_points(points, ys):
        """Create intervals from a set of start/end points."""
        assert len(ys) == len(points) * 4 - 3
        return [SimpsonInterval(a, b, ys[i:i+5]) for a, b, i in
                zip(points, points[1:], range(0, len(ys) - 1, 4))]

    @asyncio.coroutine
    def split(self):
        a, b = self.ab
        c = (a + b) / 2
        y_new = yield from asyncio.wait_for(self.future, None)
        y_new = np.asarray(y_new)
        y9 = np.empty((9,) + y_new.shape[1:], y_new.dtype)
        y9[::2] = self.y
        y9[1::2] = y_new
        return SimpsonInterval(a, c, y9[:5]), SimpsonInterval(c, b, y9[4:])

    @property
    def err(self):
        a, b = self.ab
        err = _simps_err_rule.dot(self.y) * (b - a)
        return np.max(np.abs(err))

    @property
    def val(self):
        a, b = self.ab
        return _simps_val_rule.dot(self.y) * (b - a)


_g7 = np.array([
    0.0,
    0.129484966168870, 0.0, 0.279705391489277, 0.0,
    0.381830050505119, 0.0, 0.417959183673469, 0.0,
    0.381830050505119, 0.0, 0.279705391489277, 0.0,
    0.129484966168870, 0.0]) / 2

_k15 = np.array([
    0.022935322010529, 0.063092092629979, 0.104790010322250,
    0.140653259715525, 0.169004726639267, 0.190350578064785,
    0.204432940075298, 0.209482141084728, 0.204432940075298,
    0.190350578064785, 0.169004726639267, 0.140653259715525,
    0.104790010322250, 0.063092092629979, 0.022935322010529]) / 2

_k15_points = np.array([
    -0.991455371120813, -0.949107912342759, -0.864864423359769,
    -0.741531185599394, -0.586087235467691, -0.405845151377397,
    -0.207784955007898, 0.000000000000000, 0.207784955007898,
    0.405845151377397, 0.586087235467691, 0.741531185599394,
    0.864864423359769, 0.949107912342759, 0.991455371120813])


def _gk15_xs(a, b):
        # Gauss-Kronrod xs are in the interval (-1, 1)
    c = (a + b) / 2
    return _k15_points * (b - a) / 2 + c


class GK15Interval(Interval):
    """Integral evaluated using a 15(7)-point Gauss-Kronrod rule."""

    @staticmethod
    def xs_from_points(points):
        return np.hstack([_gk15_xs(a, b) for a, b in zip(points, points[1:])])

    @staticmethod
    def from_points(points, ys):
        assert len(ys) == 15 * (len(points) - 1)
        # *ys.shape[1:] necessary when integrating vector-valued functions
        ys = np.asarray(ys)
        ys = ys.reshape(-1, 15, *ys.shape[1:])
        return [GK15Interval(a, b, y) for a, b, y in zip(points, points[1:], ys)]

    @property
    def new_xs(self):
        a, b = self.ab
        c = (a + b) / 2
        return np.hstack([_gk15_xs(a, c), _gk15_xs(c, b)])

    @asyncio.coroutine
    def split(self):
        a, b = self.ab
        c = (a + b) / 2
        y_new = yield from asyncio.wait_for(self.future, None)
        y_new = np.array(y_new)
        n = len(y_new)
        assert not n % 2
        return (GK15Interval(a, c, y_new[:n//2]),
                GK15Interval(c, b, y_new[n//2:]))

    @property
    def err(self):
        a, b = self.ab
        return np.max(np.abs(200 * (b - a) * (_g7 - _k15).dot(self.y)) ** 1.5)

    @property
    def val(self):
        a, b = self.ab
        return _k15.dot(self.y) * (b - a)


def adaptive_simpson(ffactory, *args, **kwargs):
    """Adaptively integrate using Simpson's rule.

    Parameters
    ----------
    ffactory : callable
        The integrand. Has signature ``(prio, xs)`` where ``prio`` is an
        integer (indicating priority to the adaptive routine) and ``xs`` is an
        array of float: the abscissae to evaluate. Must return an array of
        float; the integrand evaluated at the abscissae ``xs``. The returned
        array may be 1D or 2D depending on whether the integrand is scalar or
        vector valued.
    *args, **kwargs
        Extra arguments passed through to `adaptive_integration`.
    """
    return adaptive_integration(SimpsonInterval, ffactory, *args, **kwargs)


def adaptive_gauss_kronrod(ffactory, *args, **kwargs):
    """Adaptively integrate using a Gauss-Kronrod 15(7)-point rule.

    Parameters
    ----------
    ffactory : callable
        The integrand. Has signature ``(prio, xs)`` where ``prio`` is an
        integer (indicating priority to the adaptive routine) and ``xs`` is an
        array of float: the abscissae to evaluate. Must return an array of
        float; the integrand evaluated at the abscissae ``xs``. The returned
        array may be 1D or 2D depending on whether the integrand is scalar or
        vector valued.
    *args, **kwargs
        Extra arguments passed through to `adaptive_integration`.
    """
    return adaptive_integration(GK15Interval, ffactory, *args, **kwargs)


@asyncio.coroutine
def fixed_gauss_kronrod(ffactory, a, b, *args,
                        extra=0, return_intervals=False,
                        **kwargs):
    """Integrate using a Gauss-Kronrod 15(7)-point rule over a set of intervals.

    Parameters
    ----------
    ffactory : callable
        The integrand. Has signature ``(prio, xs)`` where ``prio`` is an
        integer (indicating priority to the adaptive routine) and ``xs`` is an
        array of float: the abscissae to evaluate. Must return an array of
        float; the integrand evaluated at the abscissae ``xs``. The returned
        array may be 1D or 2D depending on whether the integrand is scalar or
        vector valued.
    a, b : float
        The left and right limits of the integration interval.
    extra : iterable of floats OR int, default: 0
        Either: extra points to be inserted between a and b.
        Or: number of regularly spaced extra points to be inserted.
    return_intervals : bool, default: False
        When True, return the final set of intervals used to evaluate the
        integral, in addition to the result and error. The intervals are
        sorted by position
    *args, **kwargs
        Unused extra arguments.
    """
    # evaluate the interval using a a fixed number of 15-point rules
    points = [a]
    try:
        # Try if "extra" is an iterable.
        points.extend(extra)
    except TypeError:
        # Treat "extra" as an integer.
        if int(extra) != extra:
            raise ValueError("The number of extra points must be an integer.")

        points = np.linspace(a, b, extra + 2)
    else:
        points.append(b)
        points.sort()
        if points[0] != a or points[-1] != b:
            raise ValueError("Extra points must lie between a and b.")

    xs = GK15Interval.xs_from_points(points)
    ys = yield from asyncio.wait_for(ffactory(_minus_inf, xs), None)
    intervals = GK15Interval.from_points(points, ys)
    result = sum(ival.val for ival in intervals)
    error = sum(ival.err for ival in intervals)

    if return_intervals:
        return result, error, list(sorted(intervals, key=lambda iv: iv.ab))
    else:
        return result, error
