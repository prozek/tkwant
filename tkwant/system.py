# -*- coding: utf-8 -*-
# Copyright 2016 tkwant authors.
#
# This file is part of tkwant.  It is subject to the license terms in the file
# LICENSE.rst found in the top-level directory of this distribution and at
# http://kwant-project.org/license.  A list of tkwant authors can be found in
# the file AUTHORS.rst at the top-level directory of this distribution and at
# http://kwant-project.org/authors.
"""Tools for extracting time-dependent parts of Kwant systems."""

import functools as ft
from collections import namedtuple
import bisect
import numpy as np
import scipy.sparse as sp

__all__ = ['time_dependent', 'extract_matrix_elements', 'extract_perturbation']

_time_dependent = '_tkwant_time_dependent'


def time_dependent(func):
    """Mark Hamiltonian value functions as being time dependent.

    A decorator used to mark Hamiltonian value functions:

    .. code:: python

        @tkwant.time_dependent
        def onsite(i, j, t, B):
            return B * t
        ...
        syst = kwant.Builder()
        syst[...] = onsite
    """
    setattr(func, _time_dependent, True)
    return func


def extract_matrix_elements(syst):
    """Extract the time-dependent matrix elements from a system.

    Parameters
    ----------
    syst : `kwant.builder.FiniteSystem`

    Yields
    ------
    (int, int)
        The site indices of the matrix elements that depend on time.
    """
    # Get onsites first
    for i, onsite in enumerate(syst.onsite_hamiltonians):
        if callable(onsite) and getattr(onsite, _time_dependent, False):
            yield (i, i)
    # Now the hoppings -- half of the edges store "kwant.builder.Other", which
    # means that we will only store the actual hoppings we need to evaluate.
    for edge_id, hopping in enumerate(syst.graph):
        hop_val = syst.hoppings[edge_id]
        if callable(hop_val) and getattr(hop_val, _time_dependent, False):
            yield hopping


# TODO: this only works for finalized builders so far -- how to extend
#       this to something more general?
def extract_perturbation(syst, args=()):
    """Extract the time-dependent perturbation to the Hamiltonian.

    The total Hamiltonian can be split into two parts: the stationary
    part and the time-dependent perturbation, i.e.  H(t) = H_0 + W(t).
    This routine extracts the W(t) part from the Kwant system that
    specifies the H(t).

    Parameters
    ----------
    syst : `kwant.builder.FiniteSystem`
        The system from which to extract the time-dependent perturbation.
    args : tuple
        The arguments to the system, excluding the "time" argument.

    Returns
    -------
    callable
        A function that can be called with a time and returns a
        matrix with the same shape as ``syst.hamiltonian_submatrix()``.
        The returned matrix is either a `~scipy.sparse.lil_matrix`,
        or an `~numpy.ndarray`, depending on the value of an optional
        boolean parameter, ``sparse``. By default a sparse matrix
        is returned. The function can also be called with a time
        and a wavefunction (sequence of complex) and will return
        the result of the matrix-vector product between the
        perturbation and the wavefunction. The callable has an
        attribute "size" that denotes the size of the Hilbert space
        on which the operator acts

    Notes
    -----
    By definition the perturbation is defined with respect to t=0.
    The perturbation is therefore zero for t<0.
    """
    orbital_slice = ft.partial(_get_orbs, syst)
    H = syst.hamiltonian
    # Get the time-dependent matrix elements, their orbital slices,
    # and the matrix element value evaluated at t=0.
    td_hamiltonian = [
        (ij, list(map(orbital_slice, ij)), H(*(ij + (0,) + args)))
        for ij in extract_matrix_elements(syst)
    ]
    nsites, _, norbs = syst.site_ranges[-1]

    def herm_conj(value):
        if hasattr(value, 'conjugate'):
            value = value.conjugate()
        if hasattr(value, 'transpose'):
            value = value.transpose()
        return value

    def mat(t, sparse):
        if sparse:
            W_t = sp.lil_matrix((norbs, norbs), dtype=np.complex)
        else:
            W_t = np.zeros((norbs, norbs), dtype=np.complex)
        if t > 0:
            for ij, (to, frm), H_0 in td_hamiltonian:
                mat_el = H(*(ij + (t,) + args)) - H_0
                W_t[slice(*to), slice(*frm)] = mat_el
                if to != frm:
                    W_t[slice(*frm), slice(*to)] = herm_conj(mat_el)
        return W_t

    def mat_vec(t, ket, out):
        do_return = out is None
        if out is None:
            out = np.zeros((norbs,), dtype=np.complex)
        if t > 0:
            for ij, (to, frm), H_0 in td_hamiltonian:
                mat_el = H(*(ij + (t,) + args)) - H_0
                out[slice(*to)] += np.dot(mat_el, ket[slice(*frm)])
                if to != frm:
                    out[slice(*frm)] += np.dot(herm_conj(mat_el),
                                               ket[slice(*to)])
        return out if do_return else None

    # document for the sake of the users
    def W(time, ket=None, out=None, sparse=True):
        """Evaluate the time-dependent perturbation to the Hamiltonian.

        Attributes
        ----------
        size : int
            The size of the Hilbert space on which this operator acts.

        Parameters
        ----------
        time : float
        ket : `~numpy.ndarray`, optional
            If provided, act with the time-dependent perturbation
            on this wavefunction.
        out : `~numpy.ndarray`, optional
            If provided, and if ``ket`` is not None, act with the
            time-dependent perturbation on ``psi`` and accumulate into ``out``.
        sparse : bool
            If True, and if ``ket`` is None, return the time-dependent
            perturbation in LIL sparse format.

        Returns
        -------
        `~numpy.ndarray`, `~scipy.sparse.lil_matrix` or `None`
            If ``ket`` is None, then return the time-dependent perturbation (as
            a `~scipy.sparse.lil_matrix` if ``sparse`` is True, else a 2D
            `~numpy.ndarray`. Otherwise, return a 1D `~numpy.ndarray` if
            ``out`` is None, else return None.
        """
        if ket is None:
            return mat(time, sparse)
        else:
            msg = ('{0} vector has length {1}, but the system '
                   'contains {2} orbitals')
            if ket.shape[0] != norbs:
                raise ValueError(msg.format('Ket', ket.shape[0], norbs))
            if out is not None and out.shape[0] != norbs:
                raise ValueError(msg.format('output', out.shape[0], norbs))
            return mat_vec(time, ket, out)

    W.size = norbs

    return W


class EvaluatedSystem(namedtuple('_EvaluatedSystem',
                                 ['hamiltonian', 'boundary_slices',
                                  'solution_validators', 'time_validators'])):

    """Hamiltonian matrix of central system + boundary conditions.

    Parameters
    ----------
    hamiltonian : `~scipy.sparse.coo_matrix`
        The Hamiltonian matrix of the central system + boundary conditions.
    boundary_slices : sequence of `slice`
        Slices into ``hamiltonian`` that project onto the boundary conditions.
    solution_validators : sequence of callable
        One callable per boundary condition. Each callable takes a single
        argument, an array representing a solution *inside the boundary
        conditions*, and returns True if the solution is valid, and False
        otherwise.
    time_validators : sequence of callable
        One callable per boundary condition. Each callable has signature
        ``(next_time)`` and return True if the given boundary condition
        can be used up to the time ``next_time``, otherwise returns False.
    """

    def solution_is_valid(self, solution):
        return all(is_valid(solution[boundary]) for is_valid, boundary
                   in zip(self.solution_validators, self.boundary_slices))

    def time_is_valid(self, time):
        return all(is_valid(time) for is_valid in self.time_validators)


def hamiltonian_with_boundaries(syst, boundaries, args=()):
    """Generate the Hamiltonian with boundary conditions attached.

    Only generate the time-independent part of the Hamiltonian.
    The boundary conditions are represented by matrices :math:`h_n`
    (one per lead) that will form a direct sum with the Hamiltonian
    of the central system, :math:`H_S`:

    .. math:: H_{tot} = H_S \oplus h_0 \oplus h_1 \oplus \cdots

    In addition, coupling terms given by the lead inter-cell
    hoppings will be added between the added boundary conditions
    and the corresponding lead interface in the central system
    Hamiltonian.

    Parameters
    ----------
    syst : `~kwant.system.FiniteSystem`
        System with leads attached.
    boundaries : sequence of `~tkwant.leads.BoundaryBase`
        The boundary conditions to attach; one per lead.

    Returns
    -------
    `EvaluatedSystem`
        The Hamiltonian of the central system, evaluated at t=0,
        with boundary conditions attached, and accompanying metadata.
    """
    # generate time-independent Hamiltonian and boundary condition
    # matrices and glue them together
    boundaries = [bc(lead, args) for lead, bc in zip(syst.leads, boundaries)]
    h_0 = syst.hamiltonian_submatrix(args=((0,) + args), sparse=True)
    h_tot = sp.block_diag([h_0] + [bdy.hamiltonian for bdy in boundaries],
                          format='lil')

    boundary_slices = []

    # couple central system to boundary conditions and vice versa
    orb_offset = h_0.shape[0]
    for lead, lead_iface, boundary in zip(syst.leads, syst.lead_interfaces,
                                          boundaries):
        V = lead.inter_cell_hopping(args=(0,) + args)
        # slices over lead interface orbitals within the lead
        num_iface_sites = lead.graph.num_nodes - lead.cell_size
        V_slices = [slice(*_get_orbs(lead, s)) for s in range(num_iface_sites)]
        # slices going *to* and *from* the boundary conditions/central system
        to_slices = [slice(*(s + orb_offset)) for s in boundary.to_slices]
        from_slices = [slice(*(s + orb_offset)) for s in boundary.from_slices]

        # iterate through all sites in the interface -- they have to
        # be treated separately, as interface sites in `syst` are not
        # necessarily grouped consecutively.
        for syst_site, V_slice in zip(lead_iface, V_slices):
            syst_slice = slice(*_get_orbs(syst, syst_site))
            # coupled *to* system, *from* boundary conditions
            for to_slice in to_slices:
                h_tot[syst_slice, to_slice] =\
                    V[:, V_slice].conjugate().transpose()
            # couple *from* system, *to* boundary conditions
            for from_slice in from_slices:
                h_tot[from_slice, syst_slice] = V[:, V_slice]
                pass

        # remember which orbitals correspond to this boundary condition
        boundary_slices.append(slice(orb_offset,
                                     orb_offset + boundary.hamiltonian.shape[0]))
        # now point to the start of the next boundary condition block
        orb_offset += boundary.hamiltonian.shape[0]

    solution_is_valid = [bdy.solution_is_valid for bdy in boundaries]
    time_is_valid = [bdy.time_is_valid for bdy in boundaries]

    return EvaluatedSystem(h_tot, boundary_slices,
                           solution_is_valid, time_is_valid)


# XXX: this is lifted straight from `kwant.physics.observables`
_inf = float('inf')


def _get_orbs(syst, site):
    """Return the first orbital of this site and the next.

    Parameters
    ----------
    syst : `~kwant.system.System`
    site : int

    Returns
    -------
    pair of integers
    """
    assert site >= 0 and site < syst.graph.num_nodes
    if not syst.site_ranges:
        raise RuntimeError('Number of orbitals not defined.\n'
                           'Declare the number of orbitals using the `norbs` '
                           'keyword argument when constructing site families')
    # Calculate the index of the run that contains the site.
    # The `inf` is needed to avoid the fenceposting problem
    run_idx = bisect.bisect(syst.site_ranges, (site, _inf)) - 1
    # calculate the slice
    first_site, norbs, orb_offset = syst.site_ranges[run_idx]
    orb = orb_offset + (site - first_site) * norbs
    return orb, orb + norbs
