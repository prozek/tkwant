# -*- coding: utf-8 -*-
# Copyright 2016 tkwant authors.
#
# This file is part of tkwant.  It is subject to the license terms in the file
# LICENSE.rst found in the top-level directory of this distribution and at
# http://kwant-project.org/license.  A list of tkwant authors can be found in
# the file AUTHORS.rst at the top-level directory of this distribution and at
# http://kwant-project.org/authors.

"""Test module for `tkwant.leads`"""

import pytest
import cmath
import tinyarray as ta
import numpy as np
import kwant

from .. import leads
from .common import (make_square, make_square_with_leads, make_simple_lead,
                     make_complex_lead, check_boundary_hamiltonian)


def test_add_voltage():
    N = 3

    def check_square_lead(syst, hop_expect, lat, args=()):
        # check lead cell was added and interface was updated
        new_cell = set(lat(-1, j) for j in range(N))
        print([site in syst for site in new_cell])
        assert all(site in syst for site in new_cell)
        assert new_cell == set(syst.leads[0].interface)
        # test value
        hops = [(lat(-1, j), lat(0, j)) for j in range(N)]
        check_hops(syst, hops, hop_expect, args)

    def check_hops(syst, hops, hop_expect, args=()):
        for hop in hops:
            for time in (0,):  # (-1, 0, 1):
                got = syst[hop](*(hop + (time,) + args))
                expected = hop_expect(time, *args)
                assert np.allclose(got, expected)
                rhop = tuple(reversed(hop))
                rgot = syst[rhop](*(rhop + (time,) + args))
                rexpected = expected.conjugate()
                assert np.allclose(rgot, rexpected)

    def raises_ValueError(t):
        raise ValueError('problem in phase function')

    def bad_return_value(t):
        return 'nonsense'

    def bad_return_shape(t):
        return np.eye(2)  # expecting scalar

    def phase(t):
        return t

    def simple_hop(t):
        return -1 * cmath.exp(1j * phase(t))  # hopping should be

    ### input errors
    lat, syst = make_square(N)
    lead = kwant.Builder(kwant.TranslationalSymmetry((-1, 0)))
    lead[(lat(0, j) for j in range(N))] = 2
    lead[lat.neighbors()] = -1
    # hopping > nearest neighbor lead cell
    lead[kwant.builder.HoppingKind((2, 0), lat)] = -1
    interface = [lat(0, j) for j in range(1, N)]
    syst.leads.append(kwant.builder.BuilderLead(lead, interface))
    # add an extra site sticking out and manually attach lead on right
    # interface contains sites in different domains: error!
    syst[lat(N, 0)] = 4
    lead = kwant.Builder(kwant.TranslationalSymmetry((1, 0)))
    lead[(lat(0, j) for j in range(N))] = 2
    lead[lat.neighbors()] = -1
    interface = [lat(N, 0)] + [lat(N - 1, j) for j in range(1, N)]
    syst.leads.append(kwant.builder.BuilderLead(lead, interface))

    with pytest.raises(ValueError) as exc:
        leads.add_voltage(syst, 0, None)
    assert str(exc.value) == 'Phase function is not callable'
    with pytest.raises(ValueError) as exc:
        leads.add_voltage(syst, 0, phase)
    assert 'hopping connects non-neighboring' in str(exc.value)
    with pytest.raises(ValueError) as exc:
        leads.add_voltage(syst, 1, phase)
    assert str(exc.value) == 'Some interface sites belong to different domains'

    ### test when Hamiltonian is constants
    lat, syst = make_square_with_leads(N)
    added = leads.add_voltage(syst, 0, phase)
    new_hop = (lat(-1, 0), lat(0, 0))  # one new hopping -- system *to* lead
    check_square_lead(syst, simple_hop, lat)
    # check correct sites are added
    assert set(added) == set(lat(-1, j) for j in range(N))
    #  phase function returns numpy scalar
    lat, syst = make_square_with_leads(N)
    leads.add_voltage(syst, 0, lambda time: np.array(phase(time)))
    check_square_lead(syst, simple_hop, lat)
    # user phase function raises error
    lat, syst = make_square_with_leads(N)
    leads.add_voltage(syst, 0, raises_ValueError)
    with pytest.raises(RuntimeError) as ctx:
        syst[new_hop](*(new_hop + (0.,)))
    msg = ('Error while evaluating user-supplied phase function '
           '"raises_ValueError": problem in phase function')
    assert str(ctx.value) == msg
    # user phase function returns nonsense
    lat, syst = make_square_with_leads(N)
    leads.add_voltage(syst, 0, bad_return_value)
    with pytest.raises(RuntimeError) as ctx:
        syst[new_hop](*(new_hop + (5.,)))  # should raise RuntimeError
    msg = ('Error while evaluating user-supplied phase function '
           '"bad_return_value"')
    assert msg in str(ctx.value)

    ### test when Hamiltonian is functions
    def phase(t, arg):
        return t

    def simple_hop(t, arg):
        return -1 * cmath.exp(1j * phase(t, arg))

    lat, syst = make_square_with_leads(N, functional=True)
    leads.add_voltage(syst, 0, phase=phase)
    check_square_lead(syst, simple_hop, lat, args=(None,))

    ### matrix onsite structure
    sigma_z = ta.array([[1, 0], [0, -1]])

    def phase_mat(t):
        return t * ta.array((1, -1))

    def mat_hop(t):
        return np.dot(np.diag(np.exp(1j * phase_mat(t))), -sigma_z)

    def trivial_phase(t):
        return t

    def trivial_hop(t):
        return np.dot(cmath.exp(1j * trivial_phase(t)), -sigma_z)

    def test_mat(phase, should_be):
        lat, syst = make_square_with_leads(N, norbs=2,
                                           mat=lambda n: sigma_z)
        leads.add_voltage(syst, 0, phase=phase)
        check_square_lead(syst, should_be, lat)

    # check when return value is 1D sequence -- the diagonal
    test_mat(phase_mat, mat_hop)
    # check when return value is true scalar
    test_mat(trivial_phase, trivial_hop)
    # check when return value is numpy scalar
    test_mat(lambda t: np.array(trivial_phase(t)), trivial_hop)

    ### nontrivial leads
    def expected(time):
        return np.dot(np.diag(np.exp(1j * phase_mat(time))), 1j * sigma_z)

    lat, syst = make_square(N, norbs=2)
    lead = kwant.Builder(kwant.TranslationalSymmetry((-2, 0)))
    tags = ((0, 1), (1, 2), (1, 0))
    lead[(lat(*t) for t in tags)] = 4 * sigma_z
    lead[lat.neighbors(2)] = -np.eye(2)
    lead2 = kwant.Builder(kwant.TranslationalSymmetry((2, 0)))
    lead2.update(lead)
    # hoppings in different directions to test different code paths
    lead[(lat(0, 1), lat(1, 2))] = 1j * sigma_z
    lead[(lat(1, 0), lat(0, 1))] = -1j * sigma_z
    syst.attach_lead(lead)
    leads.add_voltage(syst, 0, phase_mat)
    new_hops = ((lat(-2, 1), lat(-1, 0)), (lat(-2, 1), lat(-1, 2)))
    check_hops(syst, new_hops, expected)
    # functional hoppings to test all code paths
    lead2[(lat(0, 1), lat(-1, 2))] = lambda i, j, t: 1j * sigma_z
    lead2[(lat(-1, 0), lat(0, 1))] = lambda i, j, t: -1j * sigma_z
    syst.attach_lead(lead2)
    leads.add_voltage(syst, 1, phase_mat)
    new_hops = ((lat(4, 1), lat(3, 0)), (lat(4, 1), lat(3, 2)))
    check_hops(syst, new_hops, expected)


@pytest.mark.parametrize('lead_maker', [make_simple_lead, make_complex_lead])
def test_free_boundary(lead_maker):

    # test invalid construction
    pytest.raises(ValueError, leads.SimpleBoundary, 0)
    pytest.raises(ValueError, leads.SimpleBoundary, -1)
    pytest.raises(ValueError, leads.SimpleBoundary, num_cells=0)
    pytest.raises(ValueError, leads.SimpleBoundary, num_cells=-1)
    pytest.raises(ValueError, leads.SimpleBoundary, max_time=0)
    pytest.raises(ValueError, leads.SimpleBoundary, max_time=-1)
    pytest.raises(TypeError, leads.SimpleBoundary)
    pytest.raises(TypeError, leads.SimpleBoundary, num_cells=2, max_time=2)

    ncells = 10
    lead = lead_maker(kwant.lattice.square(norbs=1)).finalized()
    norbs = lead.cell_size  # 1 orbital per site
    norbs_iface = lead.graph.num_nodes - lead.cell_size  # 1 orbital per site
    # ord=2 gives us the spectral norm
    max_time = ncells / (2 * np.linalg.norm(lead.inter_cell_hopping(), ord=2))
    eps = 1E-8

    bdies = [((ncells,), {}), ((), dict(num_cells=ncells)),
             ((), dict(max_time=max_time))]

    for bdy in (leads.SimpleBoundary(*args, **kwargs) for args, kwargs in bdies):
        evald_bdy = bdy(lead)
        # check format of return arguments
        assert evald_bdy.from_slices == [ta.array([0, norbs])]
        assert evald_bdy.to_slices == [ta.array([0, norbs])]
        assert evald_bdy.solution_is_valid(None)
        assert evald_bdy.time_is_valid(max_time - eps)
        assert not evald_bdy.time_is_valid(max_time + eps)
        # check produced Hamiltonian
        check_boundary_hamiltonian(
            evald_bdy.hamiltonian, norbs, norbs_iface,
            lambda i: lead.cell_hamiltonian(),
            lambda i: lead.inter_cell_hopping())


@pytest.mark.parametrize('nbuffer_cells', [-1, 0, 10])
def test_monomial_absorbing_boundary(nbuffer_cells):
    ncells = 10
    strength = 10
    degree = 6
    lead = make_simple_lead(kwant.lattice.square(norbs=1)).finalized()
    norbs = lead.cell_size  # 1 orbital per site
    norbs_iface = lead.graph.num_nodes - lead.cell_size  # 1 orbital per site

    pytest.raises(ValueError, leads.MonomialAbsorbingBoundary,
                  0, strength, degree, buffer_cells=0)
    pytest.raises(ValueError, leads.MonomialAbsorbingBoundary,
                  ncells, strength, -1, buffer_cells=0)

    if nbuffer_cells < 0:
        pytest.raises(ValueError, leads.MonomialAbsorbingBoundary,
                      ncells, strength, degree, buffer_cells=nbuffer_cells)
        return
    else:
        bdy = leads.MonomialAbsorbingBoundary(ncells, strength, degree,
                                              buffer_cells=nbuffer_cells)

    evald_bdy = bdy(lead)

    # check format of return arguments
    assert evald_bdy.from_slices == [ta.array([0, norbs])]
    assert evald_bdy.to_slices == [ta.array([0, norbs])]
    assert evald_bdy.solution_is_valid(None)
    assert evald_bdy.time_is_valid(float('inf'))

    # check produced Hamiltonian

    def absorbing(i):
        if i < nbuffer_cells:
            return 0
        else:
            i = i - nbuffer_cells
            n = degree
            return -1j * np.eye(norbs) * ((n + 1) * strength *
                                          i**n / ncells**(n + 1))

    check_boundary_hamiltonian(
        evald_bdy.hamiltonian, norbs, norbs_iface,
        lambda i: lead.cell_hamiltonian() + absorbing(i),
        lambda i: lead.inter_cell_hopping())
