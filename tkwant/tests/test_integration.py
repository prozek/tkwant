# -*- coding: utf-8 -*-
# Copyright 2016 tkwant authors.
#
# This file is part of tkwant.  It is subject to the license terms in the file
# LICENSE.rst found in the top-level directory of this distribution and at
# http://kwant-project.org/license.  A list of tkwant authors can be found in
# the file AUTHORS.rst at the top-level directory of this distribution and at
# http://kwant-project.org/authors.

"""Test module for `tkwant.leads`"""

import math
import itertools as it
import numpy as np
import asyncio
import pytest

from .. import integration

loop = asyncio.get_event_loop()

# (function, (a, b), integral)
functions = [
    (lambda x: x**2, (-1, 1), 2 / 3),
    (lambda x: 2 * np.exp(-x**2) / math.sqrt(math.pi), (0, 1), math.erf(1)),
    (lambda x: 2 * np.exp(-x**2) / math.sqrt(math.pi), (1, 0), -math.erf(1)),
    (lambda x: np.sin(x), (0, math.pi), 2),
    (lambda x: np.array([np.sin(x), 0.1 * np.exp(x)]).T,
     (0, math.pi), (2, 0.1 * (np.exp(math.pi) - 1))),
    (lambda x: np.array([x**2, x**3, x**4, x**5]).T,
     (0, 0.1),
     ((0.1 ** np.arange(3, 7)) / np.arange(3, 7))),
]

adaptive_integrators = [
    integration.adaptive_simpson,
    integration.adaptive_gauss_kronrod,
]

fixed_integrators = [
    integration.fixed_gauss_kronrod,
]


def vectorized_factory(f):

    def future_factory(prio, xs):
        fut = asyncio.Future()
        fut.set_result(f(xs))
        return fut

    return future_factory


@pytest.mark.parametrize(
    "integrator, function",
    it.product(adaptive_integrators, functions))
def test_adaptive(integrator, function):
    f, (a, b), exact_result = function
    for error in (1E-4, 1E-6, 1E-10):
        integration = integrator(vectorized_factory(f), a, b, error, limit=-1)
        result, actual_error = loop.run_until_complete(integration)
        assert actual_error < error
        assert np.max(np.abs(result - exact_result)) < error

    # consistency check
    integration = integrator(vectorized_factory(f), a, b,
                             limit=-1, return_intervals=True)
    result, error, intervals = loop.run_until_complete(integration)
    assert np.allclose(result, sum(ival.val for ival in intervals))
    assert np.allclose(error, sum(ival.err for ival in intervals))


@pytest.mark.parametrize(
    "integrator, function",
    it.product(fixed_integrators, functions))
def test_fixed(integrator, function):
    f, (a, b), exact_result = function
    for extra_points in (0, 2, 10):
        integrate = integrator(vectorized_factory(f), a, b, extra=extra_points)
        result, error = loop.run_until_complete(integrate)
        assert np.max(abs(result - exact_result)) < max(error, 1E-14)

    # consistency check
    integrate = integrator(vectorized_factory(f), a, b, return_intervals=True)
    result, error, intervals = loop.run_until_complete(integrate)
    assert np.allclose(result, sum(ival.val for ival in intervals))
    assert np.allclose(error, sum(ival.err for ival in intervals))
    xs, ys = integration.intervals_to_integrand(intervals)


@pytest.mark.parametrize("function", functions)
def test_intervals_to_integrand(function):

    ivals = [integration.GK15Interval(0, 1, np.ones((15, 3))),
             integration.GK15Interval(1, 3, np.ones((15, 3)) * 2)]
    xs, ys = integration.intervals_to_integrand(ivals)
    assert xs.shape == (30,)
    assert ys.shape == (30, 3)
    assert np.all(xs[:15] == integration._k15_points / 2 + 0.5)
    assert np.all(xs[15:] == integration._k15_points + 2)
    assert np.all(ys[:15] == np.ones((15, 3)))
    assert np.all(ys[15:] == np.ones((15, 3)) * 2)

    rules = [
        (integration.adaptive_gauss_kronrod, integration._k15, 15),
        (integration.adaptive_simpson, integration._simps_val_rule, 5)
    ]

    f, (a, b), exact_result = function

    for integrator, weights, npoints in rules:

        integ = integrator(vectorized_factory(f), a, b, return_intervals=True)
        result, error, intervals = loop.run_until_complete(integ)
        xs, ys = integration.intervals_to_integrand(intervals)
        assert xs.shape[0] == len(intervals) * npoints
        xs = np.split(xs, len(intervals))
        ys = np.split(ys, len(intervals))
        for ival, x, y in zip(intervals, xs, ys):
            assert np.all(ival.x == x)
            assert np.all(ival.y == y)
