# -*- coding: utf-8 -*-
# Copyright 2016 tkwant authors.
#
# This file is part of tkwant.  It is subject to the license terms in the file
# LICENSE.rst found in the top-level directory of this distribution and at
# http://kwant-project.org/license.  A list of tkwant authors can be found in
# the file AUTHORS.rst at the top-level directory of this distribution and at
# http://kwant-project.org/authors.

"""Test module for `tkwant.system`"""

import numpy as np
import scipy.sparse as sp
import tinyarray as ta
import kwant
import pytest

from .. import system, leads
from .common import (make_chain, make_simple_lead, make_complex_lead,
                     make_system_with_leads, check_boundary_hamiltonian)


def test_orbital_slices():

    def check(fsyst):
        slices = (system._get_orbs(fsyst, i) for i in range(len(fsyst.sites)))
        stop = 0
        for site, (start_orb, stop_orb) in zip(fsyst.sites, slices):
            assert start_orb == stop
            stop += site.family.norbs
            assert stop_orb == stop
            assert site.family.norbs == (stop_orb - start_orb)

    # uniform families
    lat, syst = make_chain(3, norbs=1)
    check(syst.finalized())
    lat, syst = make_chain(3, norbs=2)
    check(syst.finalized())
    # mixed families
    lat = kwant.lattice.chain(name='a', norbs=1)
    lat2 = kwant.lattice.chain(name='b', norbs=2)
    syst = kwant.Builder()
    syst[map(lat, range(3))] = 2
    syst[map(lat2, range(3))] = 2 * np.eye(2)
    check(syst.finalized())


def test_extract_matrix_elements():

    def onsite(i, t):
        pass

    def hopping(i, j, t):
        pass

    td_onsite = system.time_dependent(onsite)
    td_hopping = system.time_dependent(hopping)

    lat, syst = make_chain(4)
    td_sites = (lat(0), lat(1))
    td_hops = ((lat(2), lat(1)), (lat(1), lat(0)))
    # assign time-dependent parts
    for site in td_sites:
        syst[site] = td_onsite
    for hop in td_hops:
        syst[hop] = td_hopping
    syst[lat(2)] = onsite  # non time-dependent onsite function
    syst[(lat(2), lat(3))] = hopping  # non time-dependent hopping function
    fsyst = syst.finalized()

    got = tuple(system.extract_matrix_elements(fsyst))
    assert len(got) == len(set(got))
    got = set(got)
    for site in td_sites:
        idx = fsyst.sites.index(site)
        assert (idx, idx) in got
    for hop in td_hops:
        a, b = tuple(map(fsyst.sites.index, hop))
        assert (a, b) in got


def test_extract_perturbation():

    def inner_test(fsyst, N):
        W = system.extract_perturbation(fsyst)
        H0 = fsyst.hamiltonian_submatrix(args=(0,))
        H1 = fsyst.hamiltonian_submatrix(args=(1,))
        ket = np.random.rand(N) + 1j * np.random.rand(N)
        for sparse in (True, False):
            assert np.all(W(-100, sparse=sparse) == np.zeros((N, N)))
            assert np.all(W(0, sparse=sparse) == np.zeros((N, N)))
            assert np.all(W(1, sparse=sparse) == (H1 - H0))
            assert (type(W(1, sparse=sparse)) ==
                    (sp.lil_matrix if sparse else np.ndarray))

        assert np.all(W(-100, ket) == np.zeros_like(ket))
        assert np.all(W(0, ket) == np.zeros_like(ket))
        # calculations not done in same order, only check if close
        assert np.allclose(W(1, ket), np.dot(H1 - H0, ket))
        # check with wrong size
        pytest.raises(ValueError, W, 1, np.zeros((N - 1),))
        pytest.raises(ValueError, W, 1, np.zeros((N + 1),))

    @system.time_dependent
    def td_onsite(i, time):
        return 2 + time * kwant.digest.uniform(i.tag)

    @system.time_dependent
    def td_hopping(i, j, time):
        ab = ta.array(sorted((i.tag, j.tag)))
        return -1 + time * kwant.digest.uniform(ab)

    lat = kwant.lattice.chain(norbs=1)
    N = 4

    # test whole system time-dependent
    syst = kwant.Builder()
    syst[(lat(i) for i in range(N))] = td_onsite
    syst[lat.neighbors()] = td_hopping
    inner_test(syst.finalized(), N)
    # check that error is raised if norbs not given
    lat.norbs = None
    pytest.raises(RuntimeError, system.extract_perturbation,
                  syst.finalized())
    lat.norbs = 1

    # test only a partial amount time-dependent
    syst = kwant.Builder()
    syst[(lat(i) for i in range(N))] = td_onsite
    syst[(lat(i) for i in range(0, N, 2))] = 1.
    syst[lat.neighbors()] = -1
    inner_test(syst.finalized(), N)

    # test >1 orbital per site
    sigma_0 = np.eye(2)
    sigma_y = np.array([[0, -1j], [1j, 0]])

    @system.time_dependent
    def td_onsite2(i, time):
        U = kwant.digest.uniform(i.tag)
        return 2 + time * U * sigma_y

    @system.time_dependent
    def td_hopping2(i, j, time):
        U = kwant.digest.uniform(ta.array(sorted((i.tag, j.tag))))
        return -1 + time * U * sigma_0

    @system.time_dependent
    def td_interhopping(i, j, time):
        # *from* 2 orbital lattice *to* 1 orbital lattice
        U = kwant.digest.uniform(ta.array(sorted((i.tag, j.tag))))
        return 1j * time * U * np.array([[1, 1]])

    lat2 = kwant.lattice.chain(name='b', norbs=2)

    syst = kwant.Builder()
    syst[(lat(i) for i in range(N))] = td_onsite
    syst[(lat2(i) for i in range(N))] = td_onsite2
    syst[lat.neighbors()] = td_hopping
    syst[lat2.neighbors()] = td_hopping2
    syst[kwant.builder.HoppingKind((0,), lat, lat2)] = td_interhopping
    inner_test(syst.finalized(), 2 * N + N)


@pytest.mark.parametrize('lead_maker', [make_simple_lead, make_complex_lead])
def test_hamiltonian(lead_maker):
    ncells = 10
    strength = 10
    degree = 6
    # construct
    syst = make_system_with_leads(kwant.lattice.square(norbs=1),
                                  lead_maker)
    fsyst = syst.finalized()
    boundaries = [leads.SimpleBoundary(10),
                  leads.MonomialAbsorbingBoundary(ncells, strength, degree)]
    Hext = system.hamiltonian_with_boundaries(fsyst, boundaries)

    # test shapes
    norbs_central = len(fsyst.sites)
    norbs_leads = [lead.cell_size * ncells for lead in fsyst.leads]
    assert Hext.hamiltonian.shape[0] == Hext.hamiltonian.shape[1]
    assert Hext.hamiltonian.shape[0] == norbs_central + sum(norbs_leads)

    # test absorbing regions and coupling

    def absorbing(i):
        n = degree
        return -1j * np.eye(norbs) * ((n + 1) * strength *
                                      i**n / ncells**(n + 1))

    loop = zip(fsyst.leads, fsyst.lead_interfaces, Hext.boundary_slices,
               boundaries)
    for lead, lead_interface, slc, bdy in loop:
        norbs = lead.cell_size
        norbs_iface = lead.graph.num_nodes - lead.cell_size
        V = lead.inter_cell_hopping()
        V_dag = V.conjugate().transpose()
        if isinstance(bdy, leads.MonomialAbsorbingBoundary):
            def onsite(i):
                return lead.cell_hamiltonian() + absorbing(i)
        else:
            def onsite(i):
                return lead.cell_hamiltonian()
        check_boundary_hamiltonian(Hext.hamiltonian[slc, slc],
                                   norbs, norbs_iface, onsite, lambda i: V)

        # test coupling -- uses fact that norbs=1
        for i, iface_site in enumerate(lead_interface):
            bdy_iface = slice(slc.start, slc.start + norbs)
            iface_slice = slice(iface_site, iface_site + 1)
            assert np.allclose(Hext.hamiltonian[bdy_iface, iface_slice].todense(),
                               V[:, i:(i + 1)])
            assert np.allclose(Hext.hamiltonian[iface_slice, bdy_iface].todense(),
                               V_dag[i:(i + 1), :])

    # test is_valid and should_continue
    psi_test = np.empty(Hext.hamiltonian.shape[0], dtype=complex)
    assert Hext.solution_is_valid(psi_test)  # should always return True
    vmax = np.linalg.norm(fsyst.leads[0].inter_cell_hopping(), ord=2)
    tmax = ncells / (2 * vmax)
    eps = 1E-8
    assert Hext.time_is_valid(tmax - eps)
    assert not Hext.time_is_valid(tmax + eps)
