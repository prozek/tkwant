# -*- coding: utf-8 -*-
# Copyright 2016 tkwant authors.
#
# This file is part of tkwant.  It is subject to the license terms in the file
# LICENSE.rst found in the top-level directory of this distribution and at
# http://kwant-project.org/license.  A list of tkwant authors can be found in
# the file AUTHORS.rst at the top-level directory of this distribution and at
# http://kwant-project.org/authors.

import pytest
import numpy as np
import itertools as it
import scipy.sparse as sp

from .. import kernels

no_sparse_blas = pytest.mark.skipif(not hasattr(kernels, 'SparseBlas'),
                                    reason='No sparse BLAS available.')


def random_vector(N):
    return np.random.rand(N) + 1j * np.random.rand(N)


@pytest.mark.parametrize(('kernel_type',), [
    (kernels.Scipy,),
    (kernels.Simple,),
    no_sparse_blas((getattr(kernels, 'SparseBlas', None),))
])
def test_kernel(kernel_type):
    N = 1000

    H_simple = sp.eye(N, dtype=complex, format='csr')
    H_rand = sp.rand(N, N, format='csr') + 1j * sp.rand(N, N, format='csr')

    for psi_st in (None, np.zeros((N,), dtype=complex), random_vector(N)):
        for H, W in it.product((H_simple, H_rand), repeat=2):

            def W_func(time, psi, out=None):
                result = W.dot(psi)
                if out is not None:
                    out = np.asarray(out)
                    out[:] += result
                else:
                    return result

            W_func.size = N
            psi = random_vector(N)
            nonstatic = psi if psi_st is None else psi + psi_st
            should_be = -1j * (H.dot(psi) + W.dot(nonstatic))

            py_kern = kernel_type(H, W_func, psi_st=psi_st)
            c_kern = kernels.extract_c_kernel(py_kern)
            for kern in (py_kern, c_kern):
                dpsidt = random_vector(N)  # this will be overwritten anyway
                nevals = kern.nevals
                kern.rhs(psi, dpsidt, 0)
                assert kern.nevals == nevals + 1
                assert np.allclose(should_be, dpsidt)

            # now test with psi_st smaller than the total size; this will be
            # the case when we have a system with added boundary conditions.

            central_norbs = N // 2
            W_part = W.tolil()[:central_norbs, :central_norbs].tocsr()
            if psi_st is not None:
                psi_st_part = psi_st[:central_norbs]
            else:
                psi_st_part = None

            def W_func(time, psi, out=None):
                result = W_part.dot(psi)
                if out is not None:
                    out = np.asarray(out)
                    out[:] += result
                else:
                    return result

            W_func.size = central_norbs

            psi = random_vector(N)
            central_psi = psi[:central_norbs]
            should_be = H.dot(psi)
            nonstatic = (central_psi if psi_st is None else
                         central_psi + psi_st_part)
            should_be[:central_norbs] += W_part.dot(nonstatic)
            should_be *= -1j

            py_kern = kernel_type(H, W_func, psi_st=psi_st_part)
            c_kern = kernels.extract_c_kernel(py_kern)
            for kern in (py_kern, c_kern):
                dpsidt = random_vector(N)  # this will be overwritten anyway
                kern.rhs(psi, dpsidt, 0)
                assert np.allclose(should_be, dpsidt)
