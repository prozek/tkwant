# -*- coding: utf-8 -*-
# Copyright 2016 tkwant authors.
#
# This file is part of tkwant.  It is subject to the license terms in the file
# LICENSE.rst found in the top-level directory of this distribution and at
# http://kwant-project.org/license.  A list of tkwant authors can be found in
# the file AUTHORS.rst at the top-level directory of this distribution and at
# http://kwant-project.org/authors.

import pytest
from math import cos, sin
import itertools as it
import functools as ft
import numpy as np
import tinyarray as ta
import scipy.integrate._ode as ode
import scipy.sparse.linalg as spla
import kwant

from ... import system, leads
from .. import solvers, kernels, solve, restart


solvers = {
    'scipy': (
        solvers.Scipy,
        [
            dict(integrator=ode.dopri5),
            dict(integrator=ode.dop853),
        ]
    ),
    'dormand-prince': (
        solvers.DormandPrince,
        [
            dict(integrator='dopri5'),
            dict(integrator='dop853'),
        ]
    ),
}


available_kernels = {
    'scipy': kernels.Scipy,
    'simple': kernels.Simple,
    'sparseblas': getattr(kernels, 'SparseBlas', None)
}


def get_solver_kernel(solver_t, kernel_t):
    skip = False
    reasons = []
    if kernel_t in ('sparseblas',):
        if not hasattr(kernels, 'SparseBlas'):
            skip = True
            reasons.append('No sparse BLAS available.')
    skip = pytest.mark.skipif(skip, reason=', '.join(reasons))

    kernel_t = available_kernels[kernel_t]
    solver_t, solver_params = solvers[solver_t]

    return [skip((ft.partial(solver_t, **p), kernel_t)) for p in solver_params]


def all_solvers_kernels():
    chain = it.chain.from_iterable
    return pytest.mark.parametrize(
        ('solver_type', 'kernel_type'),
        list(chain(it.starmap(get_solver_kernel,
                              it.product(solvers.keys(), available_kernels.keys()))))
    )


def make_simple_lead(lat, N):
    I = ta.identity(lat.norbs)
    syst = kwant.Builder(kwant.TranslationalSymmetry((-1, 0)))
    syst[(lat(0, j) for j in range(N))] = 4 * I
    syst[lat.neighbors()] = -1 * I
    return syst


def make_complex_lead(lat, N):
    I = ta.identity(lat.norbs)
    syst = kwant.Builder(kwant.TranslationalSymmetry((-1, 0)))
    syst[(lat(0, j) for j in range(N))] = 4 * I
    syst[kwant.HoppingKind((0, 1), lat)] = -1 * I
    syst[(lat(0, 0), lat(1, 0))] = -1j * I
    syst[(lat(0, 1), lat(1, 0))] = -1 * I
    syst[(lat(0, 2), lat(1, 2))] = (-1 + 1j) * I
    return syst


def make_system(lat, N):

    I = ta.identity(lat.norbs)

    def random_onsite(site, time, salt):
        return (4 + kwant.digest.uniform(site.tag, salt=salt)) * I

    syst = kwant.Builder()
    syst[(lat(i, j) for i in range(N) for j in range(N))] = random_onsite
    syst[lat.neighbors()] = -1 * I
    return syst


def make_td_system(lat, N, td_onsite):
    I = ta.identity(2)
    syst = kwant.Builder()
    square = it.product(range(N), range(N))
    syst[(lat(i, j) for i, j in square)] = td_onsite
    syst[lat.neighbors()] = -1 * I
    return syst


def make_system_with_leads(lat, N, lead_maker):
    syst = make_system(lat, N)
    syst.attach_lead(lead_maker(lat, N))
    syst.attach_lead(lead_maker(lat, N).reversed())
    return syst


def make_td_system_with_leads(lat, N, lead_maker, td_onsite):
    syst = make_td_system(lat, N, td_onsite)
    syst.attach_lead(lead_maker(lat, N))
    syst.attach_lead(lead_maker(lat, N).reversed())
    return syst


@all_solvers_kernels()
def test_finite_time_independent(solver_type, kernel_type):
    N = 10
    salt = '1'
    lat = kwant.lattice.square(norbs=2)
    syst = make_system(lat, N)
    fsyst = syst.finalized()

    ### Start from eigenstate
    # no time dependence and starting from an eigenstate
    # φ means that we should evolve to φ * exp(-1j * E * t)
    H0 = fsyst.hamiltonian_submatrix(args=(0, salt))
    evals, evecs = spla.eigsh(H0)
    psi_st = evecs[:, 0]
    E = evals[0]
    # Solve "trivially" using (H0-E) @ psibar + W(t) @ psibar.
    # The actual ODE that we solve is 0=0, as W(t) = 0 for this system.
    psi = solve(fsyst, args=(salt,), psi_init=psi_st, energy=E,
                solver_type=solver_type, kernel_type=kernel_type)
    # Test that we get the same result when we use (H0 + W(t)) * psi directly.
    # We can do this because the system is *finite*
    psi_alt = solve(fsyst, psi_init=psi_st, args=(salt,),
                    solver_type=solver_type, kernel_type=kernel_type)
    for time in (0., 1., 5.):
        assert np.allclose(psi(time), psi_st * np.exp(-1j * E * time))
        assert np.allclose(psi_alt(time), psi(time))

    ### test restarting a calculation
    psi_rand = (np.random.random((H0.shape[0],)) +
                1j * np.random.random((H0.shape[0],)))
    for psi_init, E in [(psi_rand, None), (evecs[:, 0], evals[0])]:
        psi = solve(fsyst, psi_init, args=(salt,), energy=E,
                    solver_type=solver_type, kernel_type=kernel_type)
        psi(2)
        saved = psi.save_state()
        psi2 = restart(fsyst, saved, args=(salt,))
        assert np.allclose(psi(5), psi2(5))


@all_solvers_kernels()
def test_finite_time_dependent(solver_type, kernel_type):
    N = 10
    salt = '1'
    lat = kwant.lattice.square(norbs=2)
    I = ta.identity(2)
    SX = ta.array([[0, 1], [1, 0]])
    uniform = kwant.digest.uniform

    @system.time_dependent
    def td_onsite(site, time, salt):
        static_part = (4 + uniform(site.tag, salt=salt)) * I
        td_part = uniform(site.tag, salt=salt + '1') * SX * cos(time)
        return static_part + td_part

    syst = make_td_system(lat, N, td_onsite)
    fsyst = syst.finalized()
    nsites, _, norbs = fsyst.site_ranges[-1]
    sx_op = kwant.operator.Density(fsyst, np.array([[0, 1], [1, 0]]))

    H0 = fsyst.hamiltonian_submatrix(args=(0, salt))
    evals, evecs = spla.eigsh(H0)
    psi_st = evecs[:, 0]
    E = evals[0]

    ### system has s_z symmetry: test for conservation of s_z
    psi = solve(fsyst, psi_init=psi_st, energy=E, args=(salt,),
                solver_type=solver_type, kernel_type=kernel_type)
    density = sx_op(psi(0))
    should_be = np.sum(density)
    for time in (1., 5., 10.):
        density = sx_op(psi(0))
        assert np.isclose(
            should_be,
            np.sum(density)
        )


@all_solvers_kernels()
def test_infinite_time_independent(solver_type, kernel_type):
    N = 10
    tmax = 5
    eps = 1E-8
    salt = '1'
    E = 1.
    lat = kwant.lattice.square(norbs=2)
    syst = make_system_with_leads(lat, N, make_simple_lead)
    fsyst = syst.finalized()
    norbs = fsyst.site_ranges[-1][-1]

    boundaries = [leads.SimpleBoundary(max_time=tmax)] * 2

    ### Starting from the trivial state
    # solution should be zero everywhere
    zeros = np.zeros((norbs,), complex)
    psi = solve(fsyst, zeros, args=(salt,), boundaries=boundaries,
                solver_type=solver_type, kernel_type=kernel_type)
    for time in (0., tmax / 4, tmax / 2, tmax - eps):
        assert np.allclose(psi(time), np.zeros((norbs,)))
    pytest.raises(RuntimeError, psi, tmax + eps)

    ### Start from eigenstate
    # No time dependence and starting from an eigenstate
    # φ means that we should evolve to φ * exp(-1j * E * t).
    # The actual ODE that we solve is 0=0, as W(t) = 0 for this system.
    # Call to Kwant requires time arg to be passed explicitly
    psi_st = kwant.wave_function(fsyst, energy=E, args=(0, salt))(0)[0]
    psi = solve(fsyst, psi_st, args=(salt,), boundaries=boundaries, energy=E,
                solver_type=solver_type, kernel_type=kernel_type)
    for time in (0., tmax / 4, tmax / 2, tmax - eps):
        assert np.allclose(psi(time), psi_st * np.exp(-1j * E * time))

    norbs = fsyst.site_ranges[-1][-1]
    psi_random = np.random.random((norbs,)) + 1j * np.random.random((norbs,))

    ### test that boundary conditions are OK by testing that solutions
    ### match when the boundary conditions are extended
    extended_boundaries = [leads.SimpleBoundary(max_time=2 * tmax)] * 2
    psi = solve(fsyst, psi_random, boundaries=boundaries, args=(salt,),
                solver_type=solver_type, kernel_type=kernel_type)
    psi_ext = solve(fsyst, psi_random, boundaries=extended_boundaries, args=(salt,),
                    solver_type=solver_type, kernel_type=kernel_type)
    size = psi.psibar.shape[0]  # largest subset of psi_ext and psi
    for time in (0., tmax / 4, tmax / 2, tmax - eps):
        assert np.allclose(psi_ext(time)[:size], psi(time)[:size])

    ### test restarting a calculation
    for psi_k, E in [(psi_random, None), (psi_st, E)]:
        psi = solve(fsyst, psi_k, boundaries=boundaries, args=(salt,), energy=E,
                    solver_type=solver_type, kernel_type=kernel_type)
        psi(tmax / 2)  # evolve forward a bit
        saved = psi.save_state()
        psi2 = restart(fsyst, saved, boundaries=boundaries, args=(salt,))
        assert np.allclose(psi(tmax - eps), psi2(tmax - eps))


@all_solvers_kernels()
def test_infinite_time_dependent(solver_type, kernel_type):
    N = 10
    salt = '1'
    lat = kwant.lattice.square(norbs=2)
    I = ta.identity(2)
    SZ = ta.array([[1, 0], [0, -1]])
    uniform = kwant.digest.uniform

    @system.time_dependent
    def td_onsite(site, time, salt):
        static_part = (4 + uniform(site.tag, salt=salt)) * I
        td_part = SZ * sin(time)
        return static_part + td_part

    def phi_t(time, salt):
        return (1 - cos(time)) * ta.array([1, -1])  # diagonal part

    ### test when adding time-dependent voltage everywhere
    syst = make_td_system_with_leads(lat, N, make_simple_lead, td_onsite)
    # add TD voltage to leads as well
    leads.add_voltage(syst, 0, phi_t)
    leads.add_voltage(syst, 1, phi_t)
    # finalize
    fsyst = syst.finalized()
    nsites, _, norbs = fsyst.site_ranges[-1]

    sz_op = kwant.operator.Density(fsyst, SZ)
    boundaries = [leads.SimpleBoundary(max_time=11.)] * 2
    E = 1.

    # same time-dependent voltage everywhere, there should
    # be no change in the observables
    scattering_states = kwant.wave_function(fsyst, energy=E, args=(0, salt))
    psi_st = scattering_states(0)[0]
    psi = solve(fsyst, boundaries=boundaries, args=(salt,),
                psi_init=psi_st, energy=E,
                solver_type=solver_type, kernel_type=kernel_type)
    should_be = sz_op(psi(0))
    for time in (1., 5., 10.):
        density = sz_op(psi(time))
        assert np.allclose(
            should_be,
            density
        )
