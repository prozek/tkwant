# -*- coding: utf-8 -*-
#
# Copyright 2016 tkwant authors.
#
# This file is part of tkwant.  It is subject to the license terms in the file
# LICENSE.rst found in the top-level directory of this distribution and at
# http://kwant-project.org/license.  A list of tkwant authors can be found in
# the file AUTHORS.rst at the top-level directory of this distribution and at
# http://kwant-project.org/authors.
"""Classes for solving the time-dependent Schrödinger equation."""

import numpy as np
import scipy.integrate

from . cimport kernels
from . import kernels

__all__ = ['Scipy', 'DormandPrince']


class Scipy:
    """Solve the time-dependent Schrödinger equation using `scipy.integrate`.

    This solver will currently only work with the 'dopri5' and 'dop853'
    integrators, as these are the only re-entrant ones.

    Parameters
    ----------
    kernel : `tkwant.onebody.kernels.Kernel`
    integrator : `scipy.integrate._ode.IntegratorBase`, default: dopri5
        The integrator to use with this solver.
    **integrator_options
        Options to pass when instantiating the integrator.

    See Also
    --------
    scipy.integrate.ode
    """

    _default_options = {'atol': 1E-9, 'rtol': 1E-9, 'nsteps': int(1E9)}

    def __init__(self, kernel, integrator=scipy.integrate._ode.dopri5, **integrator_options):
        self.kernel = kernel
        # allocate storage for kernel output
        self._rhs_out = np.empty((kernel.size,), dtype=complex)

        options = dict(self._default_options)
        options.update(integrator_options)
        self._integrator = integrator(**options)
        # Factor 2 because Scipy integrators expect real arrays
        self._integrator.reset(2 * self.kernel.size, has_jac=False)

    def _rhs(self, t, y):
        # Kernel expects complex, Scipy expects real
        self.kernel.rhs(y.view(complex), self._rhs_out, t)
        return self._rhs_out.view(float)

    def __call__(self, psi, time, next_time):
        if time == next_time:
            return psi
        # psi is complex, Scipy expects real
        next_psi, final_time = self._integrator.run(
            self._rhs, lambda: None, psi.view(float), time, next_time, (), ())
        if not self._integrator.success:
            raise RuntimeError('Integration failed between {} and {}'
                               .format(time, next_time))
        assert final_time == next_time
        return next_psi.view(complex)


### Dormand-Prince solver

# This is mainly here to demonstrate how to write a C-implemented solver

ctypedef void (*dopri_rhs_t)(
    const int *n,
    const double *x,
    const double *y,
    double *f,
    double *rpar,
    int *ipar,
)

ctypedef void (*dopri_solout_t)(
    const int *nr,
    const double *xold,
    const double *x,
    double *y,
    const int *n,
    double *con,
    int *icomp,
    int *nd,
    double *rpar,
    int *ipar,
    int *irtrn,
)

ctypedef void (*dopri_integrator_t)(
    const int *n,
    dopri_rhs_t fcn,
    double *x,
    double *y,
    const double *xend,
    const double *rtol,
    const double *atol,
    const int *itol,
    dopri_solout_t solout,
    const int *iout,
    double *work,
    const int *lwork,
    int *iwork,
    const int *liwork,
    double *rpar,
    int *ipar,
    int *idid,
)

cdef extern void dopri5_(
    const int *n,
    dopri_rhs_t fcn,
    double *x,
    double *y,
    const double *xend,
    const double *rtol,
    const double *atol,
    const int *itol,
    dopri_solout_t solout,
    const int *iout,
    double *work,
    const int *lwork,
    int *iwork,
    const int *liwork,
    double *rpar,
    int *ipar,
    int *idid,
)

cdef extern void dop853_(
    const int *n,
    dopri_rhs_t fcn,
    double *x,
    double *y,
    const double *xend,
    const double *rtol,
    const double *atol,
    const int *itol,
    dopri_solout_t solout,
    const int *iout,
    double *work,
    const int *lwork,
    int *iwork,
    const int *liwork,
    double *rpar,
    int *ipar,
    int *idid,
)


cdef class DormandPrince:
    """Solve the time-dependent Schrödinger equation using Dormand-Prince.

    This solver exposes the Dopri5 and Dopri853 integrators.

    Parameters
    ----------
    kernel : `tkwant.onebody.kernels.Kernel`
    integrator : str, optional
        Name of the integrator to use with this solver.
    atol, rtol : float
        Absolute and relative error tolerances
    nsteps : int
        The maximum number of steps to take in a single call to the solver.
    """

    cdef dopri_integrator_t solver
    cdef kernels.CKernel kernel

    cdef double atol, rtol
    cdef double[:] _work
    cdef int[:] _iwork

    def __init__(self, kernels.Kernel kernel, integrator='dopri5',
                 atol=1E-9, rtol=1E-9, nsteps=int(1E9)):
        if integrator == 'dopri5':
            self.solver = dopri5_
            work_size = 8  # From the dopri5 comments
        elif integrator == 'dop853':
            self.solver = dop853_
            work_size = 11  # From the dop835 comments
        else:
            raise ValueError('Integrator must be one of: {}.'
                             .format(self._available_integrators))

        self.kernel = kernels.extract_c_kernel(kernel)

        self.atol, self.rtol = atol, rtol

        # extra factor 2 because DOPRI* works with real numbers
        _work = np.zeros((work_size * 2 * self.kernel.size + 21,), dtype=float)

        _iwork = np.zeros((21,), dtype=np.int32)
        _iwork[0] = nsteps
        _iwork[2] = -1  # Do not print status messages to stdout

        self._work = _work
        self._iwork = _iwork

    @staticmethod
    cdef void rhs(const int *n, const double *x, const double *y, double *f,
                  double *rpar, int *ipar):
        # Fortran solvers allow us to pass arbitrary information in 'rpar'
        # and 'ipar', we pass a pointer to 'self'.
        cdef DormandPrince self = <DormandPrince>(<void*>ipar)
        self.kernel.c_rhs(<const void*>self.kernel.c_self,
                          <const complex*>y, <complex*>f, x[0])


    def __call__(self, complex[::1] psi, double time, double next_time):
        cdef:
            int n = 2 * psi.shape[0]  # Factor 2 for size in real numbers
            int lwork = self._work.shape[0]
            int liwork = self._iwork.shape[0]
            int itol = 0, iout = 0
            int success = 0

        psi_new = np.array(psi)
        cdef complex [::1] psi_new_view = psi_new

        self.solver(
            &n, self.rhs, &time, <double*>&psi_new_view[0], &next_time,
            &self.rtol, &self.atol, &itol,
            NULL, &iout,
            &self._work[0], &lwork, &self._iwork[0], &liwork,
            NULL, <int*>(<void*> self),
            &success)

        if success != 1:
            raise RuntimeError('Integration failed at {}'.format(time))

        return psi_new


default = Scipy
