# -*- coding: utf-8 -*-
# cython: embedsignature=True
#
# Copyright 2016 tkwant authors.
#
# This file is part of tkwant.  It is subject to the license terms in the file
# LICENSE.rst found in the top-level directory of this distribution and at
# http://kwant-project.org/license.  A list of tkwant authors can be found in
# the file AUTHORS.rst at the top-level directory of this distribution and at
# http://kwant-project.org/authors.
"""Calculating the right-hand side of the time-dependent Schrödinger equation.

This module is mainly for internal use.

The classes in this module are used for calculating the following expression:

    ..math:: -i(H_0 ψ + W(t) ψ)

where :math:`H_0` is the time-independent part of the Hamiltonian,
:math:`W(t)` is the time-dependent part, and :math:`ψ` is some vector
defined over the Hilbert space of the problem. We recognise this expression
as the right-hand side of the time-dependent Schrödinger equation (TDSE),
where the left-hand side is :math:`∂ψ/∂t`.

In practice we will want to handle many different cases, including solving the
TDSE on infinite domains, or starting the evolution from an eigenstate.  All
the forms of TDSE that we will need to handle these cases are encompassed by
the above formula. This module is only concerned with calculating expressions
of the above form, not performing the mapping from the various problems. For
more details on the mapping, see `tkwant/onebody/solver.py`.
"""

__all__ = ['Kernel', 'CKernel', 'Scipy', 'Simple',
           'extract_c_kernel', 'default']

import numpy as np
import cython
from cpython.ref cimport Py_INCREF, Py_DECREF


########## Abstract Base Classes for kernels

cdef class Kernel:
    """ABC for right-hand side of the time-dependent Schrödinger equation.

    Attributes
    ----------
    size : int
        The size of the time-independent part of the Hamiltonian. This
        also sets the size of the solution vector.
    nevals : int
        The number of times this kernel has been evaluated since its creation.
    """

    def __init__(self, H0, W, complex[:] psi_st=None):
        raise NotImplementedError()

    def rhs(self, complex[:] psi, complex[:] dpsidt, double time):
        """Evaluate the RHS of the TDSE and store the result in `dpsidt`."""
        raise NotImplementedError()


### C-level interface to Kernels (needed for C-implemented solvers)


cdef class CKernel(Kernel):
    """Base class for kernels that directly provide a C-level interface.

    Attributes
    ----------
    c_self : void*
        Pointer to a C struct that provides extra context (e.g. H0 and W)
        to the kernel.
    c_rhs : void (*)(const void* c_self, const complex* psi, complex* dpsidt,
                     double time)
        A C function that evaluates the right-hand side of the Schrödinger
        equation, and stores the result in 'dpsidt'.
    """

    @cython.boundscheck(False)
    @cython.wraparound(False)
    def rhs(self, const complex[::1] psi, complex[::1] dpsidt, double time):
        """Evaluate the RHS of the TDSE and store the result in `dpsidt`."""
        self.c_rhs(self.c_self, &psi[0], &dpsidt[0], time)
        self.nevals += 1


# C wrapped to access pure Python Kernel implementation
cdef void _wrapped_rhs(const void *self, const complex *psi, complex *dpsidt,
                       double time) except *:
    cdef size_t size = (<object>self).size
    (<object>self).rhs(<complex[:size]>psi, <complex[:size]>dpsidt, time)


cdef class PyCKernel(CKernel):
    """C interface to python-implemented kernels."""

    def __init__(self, py_kernel):
        pass

    def __cinit__(self, py_kernel):
        self.size = py_kernel.size
        self.nevals = py_kernel.nevals
        self.c_rhs = _wrapped_rhs
        self.c_self = <void*>py_kernel
        # increment the reference count to prevent garbage collection
        Py_INCREF(<object>self.c_self)

    def __dealloc__(self):
        Py_DECREF(<object>self.c_self)


cpdef CKernel extract_c_kernel(Kernel py_kernel):
    """Extract a C-level interface from a (possibly pure-python) Kernel."""
    if isinstance(py_kernel, CKernel):
        return py_kernel
    elif not callable(py_kernel.rhs):
        raise TypeError('Kernel must have a `rhs` method.')
    return PyCKernel(py_kernel)


########## Concrete kernel implementations

class Scipy(Kernel):
    """Evaluate the RHS of the Schrödinger equation using scipy sparse.

    Parameters
    ----------
    H0 : `scipy.sparse.base`
        Hamiltonian at ``t=0`` (including any boundary conditions).
    W : callable
        Time-dependent part of the Hamiltonian. Typically the object returned
        by `tkwant.system.extract_perturbation`.
    psi_st : array of complex, optional
        The wavefunction of the initial eigenstate defined over the central
        system (if starting in an initial eigenstate).

    Attributes
    ----------
    H0 : `scipy.sparse.csr_matrix`
        Hamiltonian at ``t=0`` (including any boundary conditions).
    W : callable
        Time-dependent part of the Hamiltonian. Typically the object returned
        by `tkwant.system.extract_perturbation`.
    psi_st : array of complex or `None`
        The wavefunction of the initial eigenstate defined over the central
        system (if starting in an initial eigenstate).
    size : int
        The size of the time-independent part of the Hamiltonian. This
        also sets the size of the solution vector.
    nevals : int
        The number of times this kernel has been evaluated since its creation.
    """

    def __init__(self, H0, W, complex[:] psi_st=None):
        self.H0 = H0.tocsr()
        self.W = W
        self.psi_st = psi_st
        self.size = self.H0.shape[0]
        self.nevals = 0

    def rhs(self, const complex[:] psi, complex[:] dpsidt, double time):
        """Evaluate the RHS of the TDSE and store the result in `dpsidt`."""
        centre = self.W.size
        psi_centre = np.asarray(psi[:centre])
        # H0 @ psi -> tmp
        cdef complex[:] tmp = self.H0.dot(psi)
        # W(t) @ (psi + psi_st) + tmp -> tmp
        temp1 = psi_centre if self.psi_st is None else psi_centre + self.psi_st
        self.W(time, temp1, out=tmp[:centre])
        # dpsidt = -1j * tmp
        cdef int i
        for i in range(self.size):
            dpsidt[i] = -1j * tmp[i]

        self.nevals += 1


# Example Kernel implementation in C

cdef class _CSRMatrix:
    cdef complex[:] data
    cdef int nrows, ncols
    cdef int[:] indices, indptr

    def __cinit__(self, mat):
        mat = mat.tocsr()
        self.nrows, self.ncols = mat.shape
        self.data = mat.data
        self.indices, self.indptr = mat.indices, mat.indptr

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.initializedcheck(False)
cdef _csr_spmv(_CSRMatrix A, const complex *X, complex *Y):
    """Naive implementation of CSR-format matrix-vector multiplication.

    This is taken almost verbatim from scipy.sparse.
    """
    cdef complex sum
    cdef unsigned i, jj

    for i in range(A.nrows):
       sum = Y[i]
       for jj in range(A.indptr[i], A.indptr[i+1]):
           sum += A.data[jj] * X[A.indices[jj]]
       Y[i] = sum


cdef class Simple(CKernel):
    """A C-implemented kernel using naive CSR matvec."""

    cdef:
        object W
        int center_size
        complex[:] _psi_st, _temp
        complex *temp,
        complex *psi_st
        _CSRMatrix H0

    def __init__(self, H0, W, complex[:] psi_st=None):
        pass

    def __cinit__(self, H0, W, complex[:] psi_st=None):
        self.c_self = <void*> self
        self.c_rhs = self._rhs
        self.W = W
        self.size = H0.shape[0]
        self._psi_st = psi_st
        if psi_st is None:
            self.psi_st = NULL
        else:
            self.psi_st = &psi_st[0]
        self.center_size = self.W.size
        self.H0 = _CSRMatrix(H0)
        self._temp = np.empty((self.center_size,), dtype=complex)
        self.temp = &self._temp[0]

    # RHS implemented as a static method to conform to the solver interface.
    @staticmethod
    @cython.boundscheck(False)
    @cython.wraparound(False)
    cdef void _rhs(const void *_self, const complex *psi, complex *dpsidt,
                   double time) except *:
        cdef Simple self = <Simple>_self
        cdef unsigned i
        for i in range(self.size):
            dpsidt[i] = 0
        ### H0 @ psi -> dpsidt
        _csr_spmv(self.H0, psi, dpsidt)
        ### W(t) @ psi + dpsidt -> dpsidt
        if self.psi_st == NULL:
            self.W(time, <complex[:self.center_size]>psi,
                   out=<complex[:self.center_size]>dpsidt)
        else:
            # we need to act on `temp = psi + psi_st`
            # use pre-allocated storage
            for i in range(self.center_size):
                self.temp[i] = psi[i] + self.psi_st[i]
            self.W(time, <complex[:self.center_size]>self.temp,
                   out=<complex[:self.center_size]>dpsidt)
        ### dpsidt = -1j * dpsidt
        for i in range(self.size):
            dpsidt[i] = -1j * dpsidt[i]


try:
    from ._sparse_blas_kernel import SparseBlas
    __all__.append('SparseBlas')
except ImportError as e:
    pass

default = Scipy
