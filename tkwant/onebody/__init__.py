# -*- coding: utf-8 -*-
# Copyright 2016 tkwant authors.
#
# This file is part of tkwant.  It is subject to the license terms in the file
# LICENSE.rst found in the top-level directory of this distribution and at
# http://kwant-project.org/license.  A list of tkwant authors can be found in
# the file AUTHORS.rst at the top-level directory of this distribution and at
# http://kwant-project.org/authors.
"""Tools for solving the one-body time-dependent Schrödinger equation."""

import numpy as np
from cmath import exp
import scipy.sparse as sp

import kwant

from .. import leads
from ..system import extract_perturbation, hamiltonian_with_boundaries
from . import kernels

__all__ = []

for module in ('solvers', 'kernels'):
    exec('from . import {0}'.format(module))
    __all__.append(module)

del module  # remove cruft from namespace


class Wavefunction:
    """Evolve a single-particle wavefunction in time.

    Parameters
    ----------
    time : float
        The next time at which to get the wavefunction

    Returns
    -------
    `~numpy.ndarray`
        The wavefunction at ``time``. If this wavefunction is
        for a system with leads, then the wavefunction projected
        onto the central region is returned.


    Attributes
    ----------
    psibar : `~numpy.ndarray`
        The raw solution over the system + boundary conditions.
    time : float
        The current time.
    solver : `tkwant.onebody.Solver`
        The solver used to evolve the wavefunction forward in time.

    Notes
    -----
    You should not try to create an instance of this class directly; use
    `tkwant.onebody.solve` instead.

    This is actually a wrapper around a solver that takes care of state.

    The only state that the solvers store themselves is configuration (e.g.
    numerical tolerances, which specific method to use, etc.), This class
    takes care of storing the actual solution,

    In addition, this class checks for the validity of the obtained solution,
    as well as whether the evolution can continue up to the requested time.
    Also, it converts between the raw solution obtained from solving
    the differential equation (i.e. including the physically irrelevant
    parts inside the boundary conditions) and the actual wavefunction
    projected onto the central region.
    """

    def _psi(self):
        if self._energy is None:
            return self.psibar[:self._syst_size]
        else:
            return ((self.psibar[:self._syst_size] + self._psi_st) *
                    exp(-1j * self._energy * self.time))

    def __call__(self, next_time):
        if next_time < self.time:
            raise ValueError('Cannot evolve backwards in time')
        if next_time == self.time:
            return self._psi()
        if not self._time_is_valid(next_time):
            raise RuntimeError('Cannot evolve up to {} with the given '
                               'boundary conditions'.format(next_time))
        # evolve forward in time
        next_psibar = self.solver(self.psibar, self.time, next_time)
        if not self._solution_is_valid(next_psibar):
            raise RuntimeError('Evolving between {} and {} resulted in an '
                               'unphysical result due to the boundary '
                               'conditions'.format(self.time, next_time))
        # update internal state and return
        self.psibar, self.time = next_psibar, next_time
        return self._psi()

    def save_state(self):
        """Save the state of the current wavefunction.

        Can be restored with `tkwant.onebody.restart`.
        """
        return self._psi_st, self._energy, self.psibar, self.time


def solve(syst, psi_init, *, args=(), tmax=None, boundaries=None, energy=None,
          kernel_type=kernels.default, solver_type=solvers.default):
    """Solve the time-dependent Schrödinger equation.

    Parameters
    ----------
    syst : `kwant.builder.FiniteSystem`
        The low level system for which the wave functions are to be
        calculated.
    psi_init : array of complex
        The state from which to start, defined over the central region.
    args : tuple, optional
        The extra arguments to pass to the system, excluding ``time``.
    energy : float, optional
        If provided, then ``psi_init`` is assumed to be an eigenstate
        of energy ``E``. If ``syst`` has leads, then ``psi_init`` is
        assumed to be the projection of a scattering state at energy ``E``
        on to the central part of the system.
    tmax : float, optional
        The maximum time up to which to simulate. Sets the boundary conditions
        such that they are guaranteed to be correct up to 'tmax'. Mutually
        exclusive with 'boundaries'.
    boundaries : sequence of `~tkwant.leads.BoundaryBase`, optional
        The boundary conditions for each lead attached to ``syst``. Mutually
        exclusive with 'tmax'

    Returns
    -------
    `tkwant.onebody.Wavefunction`
        An object that can be called with a time and returns the
        wavefunction at that time.
    """
    if not isinstance(syst, kwant.system.FiniteSystem):
        raise TypeError('tkwant.solve requires a FiniteSystems.')

    if tmax is None and boundaries is None and syst.leads:
        raise ValueError("'boundaries' or 'tmax' must be provided "
                         "for a system with leads.")
    if tmax is not None and boundaries is not None:
        raise ValueError("'boundaries' and 'tmax' are mutually exclusive.")
    if tmax:
        boundaries = [leads.SimpleBoundary(max_time=tmax)
                      for lead in syst.leads]

    # Total number of orbitals in the central system
    syst_size = syst.site_ranges[-1][2]
    if psi_init.shape != (syst_size,):
        raise ValueError('`psi_init` should have the same number of '
                         'orbitals as the system')

    if syst.leads:
        # need to add boundary conditions
        if not boundaries:
            raise ValueError('Must provide boundary conditions for systems '
                             'with leads.')
        # get static part of Hamiltonian for central system + boundary conditions
        # TODO: Make this work with arbitrary kwant.system.FiniteSystem.
        extended_system = hamiltonian_with_boundaries(syst, boundaries,
                                                      args=args)
        H0 = extended_system.hamiltonian
        solution_is_valid = extended_system.solution_is_valid
        time_is_valid = extended_system.time_is_valid
    else:
        # true finite systems (no leads) so no need for boundary conditions
        H0 = syst.hamiltonian_submatrix(args=((0.,) + args), sparse=True)

        def solution_is_valid(solution):
            return True

        def time_is_valid(time):
            return True

    # TODO: Make this work with arbitrary kwant.system.FiniteSystem.
    W = extract_perturbation(syst, args=args)

    if energy is None:
        # starting from an arbitrary state, so we need
        # to be solving: H0 @ psi + W(t) @ psi
        kernel = kernel_type(H0, W)
    else:
        # we are starting from an eigenstate, so we need to
        # be solving: (H0 - E) @ psibar + W(t) @ (psibar + psi_st)
        # and psi = (psibar + psi_st) * exp(-1j * energy * time)
        kernel = kernel_type(H0 - energy * sp.eye(H0.shape[0]),
                             W, np.asarray(psi_init))

    wf = Wavefunction()
    wf._solution_is_valid = solution_is_valid
    wf._time_is_valid = time_is_valid
    wf._syst_size = syst_size
    wf.solver = solver_type(kernel)
    wf.time = 0
    wf._energy = energy
    wf.psibar = np.zeros((kernel.size,), complex)
    if energy is not None:
        wf._psi_st = np.array(psi_init, complex)
    else:
        wf._psi_st = None
        wf.psibar[:syst_size] = psi_init

    return wf


def restart(syst, saved_state, *, args=(), tmax=None, boundaries=None,
            kernel_type=kernels.default, solver_type=solvers.default):
    """Restart a previous calculation from its saved state.

    Parameters
    ----------
    saved_state : tuple
        The output of `Wavefunction.save_state()`
    syst : `kwant.builder.FiniteSystem`
        The low level system for which the wave functions are to be
        calculated.
    tmax : float, optional
        The maximum time up to which to simulate. Sets the boundary conditions
        such that they are guaranteed to be correct up to 'tmax'. Mutually
        exclusive with 'boundaries', and must be the same as that originally
        used to produce 'saved_state'.
    boundaries : sequence of `~tkwant.leads.BoundaryBase`, optional
        The boundary conditions for each lead attached to ``syst``. Mutually
        exclusive with 'tmax', and must be the same as that originally used to
        produce 'saved_state'.
    """
    psi_st, energy, psibar, time = saved_state
    syst_size = syst.site_ranges[-1][2]
    # if energy is None (and therefore psi_st is None), then psi_init is
    # not used directly in `solve`, so we can initialize it to empty().
    psi_init = np.empty((syst_size), complex) if psi_st is None else psi_st
    new = solve(syst, psi_init, args=args, tmax=tmax, boundaries=boundaries,
                energy=energy)
    new.psibar, new.time = np.array(psibar), time
    return new
