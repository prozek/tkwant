# -*- coding: utf-8 -*-
# Copyright 2016 tkwant authors.
#
# This file is part of tkwant.  It is subject to the license terms in the file
# LICENSE.rst found in the top-level directory of this distribution and at
# http://kwant-project.org/license.  A list of tkwant authors can be found in
# the file AUTHORS.rst at the top-level directory of this distribution and at
# http://kwant-project.org/authors.
"""Tools for calculating time-dependent, manybody quantities."""

import sys
import traceback
import enum
from collections import namedtuple
import heapq
import functools as ft
import itertools as it
import numpy as np
import asyncio as io
from mpi4py import MPI
import kwant

from . import onebody, integration


__all__ = ['Solver']

Task = namedtuple('Task', 'energy lead mode')

_cancelled_future = io.Future()
_cancelled_future.cancel()

# "ensure_future" only introduced in Python 3.4.4
if not hasattr(io, "ensure_future"):
    io.ensure_future = io.async


class _WorkQueue:
    """Priority queues for a number of workers, with sticky work.

    Tasks may be put on to the queue, and later tasks are removed
    from the queue and assigned to workers. The queue maintains
    a memory of which tasks were assigned to which workers, so if
    the same task ever appears again, it will be assigned to the
    same worker as it was previously.

    Parameters
    ----------
    n_workers : int
        The number of work queues.
    worker_by_task : dict
        maps tasks to the worker to which they are assigned.
    """

    def __init__(self, n_workers, worker_by_task):
        self.unassigned_q = list()
        self.assigned_q = [list() for i in range(n_workers)]
        self.tasks_available = io.Event()
        # this may be shared between several queues
        self.worker_by_task = worker_by_task

    def put(self, priority, task, future):
        worker = self.worker_by_task.get(task, None)
        q = self.unassigned_q if worker is None else self.assigned_q[worker]
        heapq.heappush(q, (priority, task, future))
        self.tasks_available.set()

    def empty(self):
        return not self.tasks_available.is_set()

    @io.coroutine
    def wait(self):
        return (yield from self.tasks_available.wait())

    def assign_work(self, available_workers):
        """Assign work to available workers.

        This method implements the scheduling policy of the queue.

        Parameters
        ----------
        workers : sequence of int
            Workers that are free to have work assigned to them.

        Returns
        -------
        ``(work, workers)``
            where ``work`` is a sequence of elements like
            ``(worker, (priority, task, future))``, and ``workers`` is a
            sequence of integers for workers that were not assigned tasks.
        """
        if any(w < 0 or w >= len(self.assigned_q) for w in available_workers):
            raise ValueError('Workers must be numbered from 0 to {} exclusive.'
                             .format(len(self.assigned_q)))
        unassigned_workers = []
        work = []
        for worker in available_workers:
            for q in (self.assigned_q[worker], self.unassigned_q):
                # get the first non-cancelled task
                future = _cancelled_future
                try:
                    while future.cancelled():
                        priority, task, future = heapq.heappop(q)
                except IndexError:
                    # no non-cancelled tasks in this queue; go to next queue
                    continue
                # we found a non-cancelled task; go to next worker
                if q is self.unassigned_q:
                    # ensure this task goes to the same worker next time
                    self.worker_by_task[task] = worker
                work.append((worker, (priority, task, future)))
                break
            else:
                # went through all the queues and got no work
                unassigned_workers.append(worker)

        if not self.unassigned_q and not any(self.assigned_q):
            self.tasks_available.clear()

        return work, unassigned_workers


@io.coroutine
def _sum_futures(futures):
    results = yield from io.gather(*futures)
    return sum(results)


def _put_work(work_queue, leads, modes):
    """Return a callback that can be used by integrators.

    The energies requested by the integrator are turned into tasks
    and put into the work queue. This queue is shared with the
    ``master`` coroutine of the ``Solver``.

    Parameters
    ----------
    work_queue : _WorkQueue
        queue on to which to put tasks and their futures
    leads : tuple of int
    modes : tuple of int
        This information is needed to create tasks from the energies
        requested by the integrator that uses this callback.
    """

    def wrapped(prio, energies):
        # TODO: adjust priority based on error across all the integrators.
        fs = []
        for energy in energies:
            lead_mode_futures = []
            for lead, mode in it.product(leads, modes):
                future = io.Future()
                work_queue.put(prio, Task(energy, lead, mode), future)
                lead_mode_futures.append(future)
            future = io.ensure_future(_sum_futures(lead_mode_futures))
            fs.append(future)

        return io.gather(*fs)

    return wrapped


def fermi_dirac(mu, T):
    if np.isclose(T, 0):
        def fd(E):
            return 1 if E <= mu else 0
    else:
        def fd(E):
            return 1. / (1 + np.exp((E - mu) / T))
    return fd


def _get_wavefunction(onebody_solver, syst, boundaries, args, energy, lead, mode):
    scattering_states = kwant.wave_function(syst, energy, args=(0,) + args)
    psi_init = scattering_states(lead)[mode]
    return onebody_solver(syst, psi_init, boundaries=boundaries, args=args,
                          energy=energy)


def _normalize_integration_region(region):
    leads, modes, energy_intervals = region
    if not all(E0 < E1 for E0, E1 in zip(energy_intervals, energy_intervals[1:])):
        raise ValueError('Integration bounds must be sorted.')
    leads = (leads,) if isinstance(leads, int) else leads
    modes = (modes,) if isinstance(modes, int) else modes
    return leads, modes, energy_intervals


def _intervals_to_extra_points(intervals):
    """Return the `a, b` and ``extra_points`` needed to reconstruct a set of intervals"""
    intervals = it.chain.from_iterable(ival.ab for ival in intervals)
    a, *extra_points, b = sorted(set(intervals))
    return extra_points


class Solver:
    """
    Parameters
    ----------
    syst : `kwant.builder.FiniteSystem`
    boundaries : sequence of `~tkwant.leads.BoundaryBase`
        The boundary conditions for each lead attached to ``syst``.
    occupations : sequence of functions ``f(E)``
        The occupation functions for each lead attached to ``syst``.
    integration_regions : sequence of ``(lead, mode, energy_bounds)``
        Regions over which to integrate. Care must be taken when bands
        close/open and the integration regions must be split appropriately
        when bands cross. ``energy_bounds`` must be a sorted sequence.
    args : tuple, optional
        Extra arguments to pass to the Hamiltonian of ``syst``, excluding time.
    integration_error : float, optional
        Absolute error bound for the energy integration.
    integrator_factory : callable, optional
        Returns an integrator object.
    onebody_solver : callable, default: `onebody.solve`
        Solver for the one-body time-dependent Schrödinger equation.
        Must have the same signature as `onebody.solve`. This allows
        one to customize the differential equation solver and kernel
        (right-hand side) by passing
        `onebody_solver=functools.partial(onebody.solve,
                                          kernely_type=custom_kernel,
                                          solver_type=custom_solver)`.
    comm : `mpi4py.MPI.Intracomm`, optional
        The MPI communicator over which to parallelize the computation.

    Notes
    -----
    Specifying ``integration_regions`` allows for quite some flexibility, e.g.
    you may just want to calculate the contribution from certain leads
    or modes, in which case it is sufficient.

    In the simplest case `lead` and `mode` are integers. Using this mode
    of operation the contribution from the given leads/modes are integrated
    separately. Sometimes, however it may be advantageous to *sum over the
    results from a set of leads/modes before integrating*. This is usually
    the case when the given leads have identical band structures, and so
    their contributions will often cancel, making the integration easier.
    To use this mode of operation, one may provide `lead` and/or `mode`
    as a *tuple of integers*, for example::

        regions = [((0, 1), 0, (Emin, Emax))]

    would indicate that we wanted to sum over mode 0 of leads 0 and 1,
    whereas::

        regions = [((0, 1), (0, 1), (Emin, Emax))]

    would indicate that we wanted to sum over modes 0 and 1 of leads
    0 and 1 (both modes should, of course, be open at all energies
    between ``Emin`` and ``Emax``).

    The ``energy_bounds`` element of the itegration regions must contain
    at least 2 elements: the min and max energy bounds. It may, however,
    contain more elements, in which case the integral over this band
    will be further split according to the points provided::

        regions = [0, 1, (Emin, Emid, Emax)]
    """

    def __init__(self, syst, boundaries, occupations, integration_regions, args=(),
                 *,
                 integration_error=1E-6,
                 integrator_factory=integration.adaptive_gauss_kronrod,
                 onebody_solver=onebody.solve,
                 comm=MPI.COMM_WORLD):

        if len(occupations) != len(syst.leads):
            raise ValueError('An occupation function must be provided for '
                             'each lead.')

        integration_regions = list(map(_normalize_integration_region,
                                       integration_regions))

        self.comm = comm

        if mpi_am_master(comm):
            self.integrator_factory = integrator_factory
            self.integration_error = integration_error
            # Store the worker (MPI rank) associated with each task across
            # calls to the solver/
            self.worker_by_task = {}
            self.integration_regions = integration_regions
            # extra points on which to split integrals
            self.extra_integration_points = [
                extra for _, _, (_, *extra, _) in self.integration_regions]

        if mpi_am_worker(comm):
            self.args = args
            self.get_wavefunction = ft.partial(
                _get_wavefunction, onebody_solver, syst, boundaries, args)
            # set the occupation functions
            self.occupations = occupations
            # Store the time-evolved wavefunction for each task across
            # calls to the solver.
            self.state_by_task = {}

    def __call__(self, observable, time, return_extra_info=False):
        """Evaluate an observable at a point in time.

        Parameters
        ----------
        observable : callable or Kwant operator
            If callable, has signature ``(psi, time, *args)`` where
            ``args`` are the extra arguments to the system Hamiltonian
            (excluding time). Returns either a scalar or a numpy array.
        time : float
            Time at which to evaluate the observable.
        return_extra_info: bool, default: False
            If True, return ``(result, error, intervals)``, where ``error`` is
            the error on ``result``, and ``intervals`` is the sequence
            of `tkwant.integration.Interval`s that were used to compute
            the integral.

        Returns
        -------
        The expectation value of ``observable``, integrated over all
        occupied bands.

        Notes
        -----
        Returns ``None`` on all non-zero MPI ranks.
        """

        # wrap Kwant operator so that it has call signature (psi, time, *args)
        if isinstance(observable, kwant.operator._LocalOperator):
            _observable = observable

            def observable(psi, time, *args):
                return _observable(psi, args=(time,) + args)

        fs = []
        if mpi_am_master(self.comm):
            # Work queue that always assigns a given task to the same worker.
            # Workers identified by MPI rank, so tell the queue that we have 1
            # more worker than we do (simplifies logic when master == worker)
            work_queue = _WorkQueue(self.comm.size, self.worker_by_task)

            # calculate the absolute error tolerance for each interval
            # (just divide total error tolerance by the relative size
            # of each interval).
            # TODO: change this when the integrator is changed to have
            #       a different function per interval.
            Etot = sum(hi - lo for *_, (hi, lo) in self.integration_regions)
            region_errors = [(hi - lo) * self.integration_error / Etot
                             for *_, (hi, lo) in self.integration_regions]

            # Integrator coroutines.
            # These produce energies at which to evaluate observables.
            # '_put_work' takes the energies that the integrators need
            # and produces triples (energy, lead, mode) that can be
            # given to a single worker to evaluate. Results are communicated
            # back to the integrators by Futures.
            integrators = [
                self.integrator_factory(
                    _put_work(work_queue, lead, mode),
                    Emin, Emax, error,
                    extra=extra,
                    return_intervals=True)
                for (lead, mode, (Emin, *_, Emax)), extra, error in
                zip(self.integration_regions,
                    self.extra_integration_points,
                    region_errors)
            ]
            integrators = io.gather(*integrators)
            fs.append(integrators)

            # Master coroutine.
            # Pulls work (supplied by the integrators) off the queue and
            # sends it to the appropriate worker. Receives results from
            # workers and sets the associated future; this will wake
            # up the appropriate integrator routine, which will fill
            # the work queue with more work.
            master = self.master(work_queue, integrators)
            fs.append(master)

        if mpi_am_worker(self.comm):
            # Worker coroutine.
            # Receives messages from the master coroutine and evaluates
            # the observable on a given wavefunction (specified by
            # the triple (energy, lead, mode)).
            fs.append(self.worker(observable, time))

        # run everything
        loop = io.get_event_loop()
        try:
            loop.run_until_complete(io.gather(*fs))
        except Exception:
            print(traceback.format_exc(), file=sys.stderr)
            self.comm.Abort()

        if mpi_am_master(self.comm):
            # get all the results of the different regions
            results, errors, regions = zip(*integrators.result())
            # next time we are called, start directly from the intervals
            # we used used to evaluate the integrals *this* time
            self.extra_integration_points = [
                _intervals_to_extra_points(ivals) for ivals in regions]
            # sum all the results and errors from the different regions
            if return_extra_info:
                return sum(results), sum(errors), regions
            else:
                return sum(results)

    @io.coroutine
    def master(self, work_queue, integrators):
        """Run the master process that drives workers.

        Parameters
        ----------
        work_queue : _WorkQueue
            Prioritized work queue with sticky work.
        integrators : `asyncio.Future`
            Tracks completion of the integrator coroutines.
            We should exit when this completes.
        """
        assert mpi_am_master(self.comm)
        waiting_workers = list(mpi_worker_ranks(self.comm))
        # Workers identified by MPI rank, so make the list 1 larger than
        # strictly necessary (also simplifies logic when master == worker)
        pending_results = [None] * self.comm.size  # indexed by worker rank
        # Wait for integrator to fill task queue with initial work.
        yield from work_queue.wait()

        ### main loop
        while not integrators.done():
            ### send tasks to run
            if not work_queue.empty():
                # Schedule available work onto waiting workers.
                to_send, waiting_workers = work_queue.assign_work(waiting_workers)
                reqs = []
                for worker, (prio, task, future) in to_send:
                    reqs.append(mpi_send(self.comm, task, dest=worker,
                                         msg_type=Message.task))
                    pending_results[worker] = future
                # XXX: Not really necessary to wait for the send to complete
                yield from mpi_wait(self.comm, *reqs)

            ### receive a single result
            req = mpi_recv(self.comm, msg_type=Message.result)
            (worker, (success, result)), = yield from mpi_wait(self.comm, req)
            future = pending_results[worker]
            pending_results[worker] = None
            waiting_workers.append(worker)  # mark worker as available
            if success:
                if not future.cancelled():
                    future.set_result(result)
                # Give the integrators a chance to react to the result.
                # XXX: Should only yield when we would unblock an integrator
                yield
            else:
                # fail bitterly if there was a problem on a worker
                exc, tb = result
                print(tb, file=sys.stderr)
                self.comm.Abort()

            ### check end condition
            if not any(pending_results) and work_queue.empty():
                # all results received and no tasks in queue; wait for
                # integrators to finish or give us more work to do.
                # XXX: this may unblock after only a single integrator has
                #      pushed work into the task queue -- not optimal!
                wait_on_q = io.async(work_queue.wait())
                done, _ = yield from io.wait((integrators, wait_on_q),
                                             return_when=io.FIRST_COMPLETED)
                if wait_on_q not in done:
                    wait_on_q.cancel()

        # sanity checks -- all remaining futures are cancelled
        assert all(f is None or f.cancelled() for f in pending_results)
        assert all(f.cancelled() for _, _, f in work_queue.unassigned_q)
        assert all(f.cancelled() for tl in work_queue.assigned_q for _, _, f in tl)
        # All cancelled futures in `pending_results` are still running on the
        # workers -- we need to post `recv`s to match the `send`s (even though
        # we will just discard the results).
        working_workers = set(mpi_worker_ranks(self.comm)) - set(waiting_workers)
        reqs = [mpi_recv(self.comm, source=worker, tag=Message.result)
                for worker in working_workers]
        yield from mpi_wait(self.comm, *reqs)
        # kill workers
        reqs = [mpi_send(self.comm, None, dest=worker, msg_type=Message.task)
                for worker in mpi_worker_ranks(self.comm)]
        yield from mpi_wait(self.comm, *reqs)

    @io.coroutine
    def worker(self, observable, time):
        """Run the worker that processes tasks.

        Parameters
        ----------
        observable : callable
            Takes ``(psi, time)`` where ``psi`` is the wavefunction
            defined over the system at the present time, and returns
            a scalar or array.
        time : float

        Notes
        -----
        If the observable returns an array, the integrator routine
        used must support vector integrands.
        """
        assert mpi_am_worker(self.comm)
        while True:
            req = mpi_recv(self.comm, source=0, msg_type=Message.task)
            (_, task), = yield from mpi_wait(self.comm, req)
            if task is None:  # `None` is the signal that we should die
                break
            # evolve the wavefunction forward in time and evaluate observable
            try:
                if task in self.state_by_task:
                    # wavefunction exists for this task
                    psi = self.state_by_task[task]
                else:
                    try:
                        psi = self.get_wavefunction(task.energy, task.lead, task.mode)
                    except ValueError as exc:
                        # if we hit a band edge Kwant will complain. In such
                        # a case we should just return 0
                        if 'expected square matrix' not in exc.args[0]:
                            raise
                        psi = None
                    self.state_by_task[task] = psi

                if psi is not None:
                    result = observable(psi(time), time, *self.args)
                    result *= self.occupations[task.lead](task.energy) / (2 * np.pi)
                else:
                    result = 0

                success = True

            except Exception as e:
                success = False
                result = (e, traceback.format_exc())

            req = mpi_send(self.comm, (success, result), dest=0,
                           msg_type=Message.result)
            yield from mpi_wait(self.comm, req)

    def task_distribution(self):
        """Get the number of tasks assigned to each rank.

        Notes
        -----
        Returns ``None`` on all non-zero ranks.
        """
        if mpi_am_master(self.comm):
            task_dist = [0] * self.comm.size
            for worker in self.worker_by_task.values():
                task_dist[worker] += 1
            return task_dist


### MPI tools

def mpi_am_master(comm):
    return comm.rank == 0


def mpi_am_worker(comm):
    return comm.size == 1 or comm.rank != 0


def mpi_worker_ranks(comm):
    return (0,) if comm.size == 1 else range(1, comm.size)


class Message(enum.IntEnum):
    task = 1
    result = 2


def mpi_recv(comm, msg_type, source=MPI.ANY_SOURCE):
    return comm.irecv(source=source, tag=msg_type)


def mpi_send(comm, message, dest, msg_type):
    return comm.isend(message, dest=dest, tag=msg_type)


@io.coroutine
def mpi_wait(comm, *requests):
    statuses = [MPI.Status() for _ in requests]
    if mpi_am_master(comm) and mpi_am_worker(comm):
        while True:
            complete, responses = MPI.Request.testall(requests, statuses)
            if complete:
                break
            yield
    else:
        responses = MPI.Request.waitall(requests, statuses)
    return [(s.source, r) for r, s in zip(responses, statuses)]
