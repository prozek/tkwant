# -*- coding: utf-8 -*-
# Copyright 2016 tkwant authors.
#
# This file is part of tkwant.  It is subject to the license terms in the file
# LICENSE.rst found in the top-level directory of this distribution and at
# http://kwant-project.org/license.  A list of tkwant authors can be found in
# the file AUTHORS.rst at the top-level directory of this distribution and at
# http://kwant-project.org/authors.
"""Tools for dealing with time-dependent infinite, periodic leads.

Also contains tools for dealing with boundary conditions that simulate
the effect of leads when solving the time-dependent Schrödinger equation.
"""


import abc
import cmath
import bisect
import functools as ft
from collections import namedtuple
from scipy import sparse as sp
import tinyarray as ta
import numpy as np

from . import system

__all__ = ['add_voltage', 'SimpleBoundary', 'MonomialAbsorbingBoundary']


# TODO: change this when Kwant can have further than nearest-neighbor
#       hoppings in leads
def add_voltage(syst, lead, phase):
    r"""Add a time-dependent voltage to a lead.

    Add a lead unit cell to the ``syst`` where ``lead`` is
    attached, and couple this to ``syst`` with a time-dependent
    hopping that effectively adds a voltage to the lead.

    Parameters
    ----------
    syst : `kwant.builder.Builder`
        The central system, with its leads already attached.
        Modified on return.
    lead : int
        The number of the lead on which to apply the voltage.
    phase : callable
        Function specifying the anti-derivative of the voltage.
        Takes takes the same extra arguments as the
        Hamiltonian value functions, starting with the time.
        Returns either a scalar or a one-dimensional sequence:
        one element per orbital on a site. Must return 0
        (or a sequence of zeros) at time 0.

    Returns
    -------
    tuple
        The sites added to the system.

    Raises
    ------
    ValueError
        If the ``phase`` argument is not callable, or if sites in the
        lead interface belong to different domains

    Notes
    -----
    Formally, this function adds a single lead cell to the
    system and attaches the previous lead interface to it with
    a hopping :math:`(1 \otimes \exp[iX(t)])V`, where V is the hopping
    between lead unit cells, 1 is the unit matrix over the interface sites
    and X(t) is a square diagonal matrix over the orbitals on
    one site, specified by the ``phase`` parameter.  This
    corresponds to adding a voltage X'(t) to each site in the
    lead. In the most common case of 1 orbital per site X(t) is
    a scalar.

    This function only works if the lead was attached with ``attach_lead``,

    If any exceptions are raised in this function there are no
    guarantees that the system was not modified.
    """
    lead_builder = syst.leads[lead].builder
    interface = syst.leads[lead].interface
    sym = lead_builder.symmetry
    interface_dom = sym.which(interface[0])
    # check for valid inputs
    if not callable(phase):
        raise ValueError('Phase function is not callable')
    # sanity check that interface sites are all in the same domain,
    # in any case, if this is not true Kwant will not be able to
    # finalise the system
    if not all(sym.which(s) == interface_dom for s in interface):
        raise ValueError('Some interface sites belong to different domains')
    # check that there are not hoppings > nearest neighbor cells
    for hopping in lead_builder.hoppings():
        if not -1 <= sym.which(hopping[1])[0] <= 1:
            msg = ('The following hopping connects non-neighboring lead '
                   'unit cells. Only nearest-cell hoppings are allowed '
                   '(consider increasing the lead period).\n{0}')
            raise ValueError(msg.format(hopping))

    # phase *from* the system *to* the lead (new cell)
    phase_factor = _phase_factor_function(phase)
    # called with a hopping and whether this is *from* the system *to*
    # the lead (new cell) or vice versa, returns a hopping value function
    time_dependent_hopping = ft.partial(_time_dependent_hopping, phase_factor)

    # map sites to domain of the new cell
    def move(x):
        return sym.act(interface_dom + 1, sym.act(-sym.which(x), x))

    # copy over sites
    for site in lead_builder.H:
        syst[move(site)] = lead_builder[site]

    # copy over hoppings
    for (to_site, from_site), value in lead_builder.hopping_value_pairs():
        if sym.which(to_site) == sym.which(from_site):  # intra-cell hopping
            syst[move(to_site), move(from_site)] = value
        else:  # inter-cell hopping
            system_to_lead = sym.which(to_site)[0] > sym.which(from_site)[0]
            if system_to_lead:
                from_site = sym.act(interface_dom - sym.which(from_site), from_site)
                to_site = move(to_site)
            else:
                to_site = sym.act(interface_dom - sym.which(to_site), to_site)
                from_site = move(from_site)
            syst[to_site, from_site] = time_dependent_hopping(value, system_to_lead)

    syst.leads[lead].interface = tuple(map(move, interface))
    return tuple(map(move, lead_builder.H))


########## Boundary Conditions

class EvaluatedBoundary(namedtuple('_System',
                                   ['hamiltonian', 'to_slices', 'from_slices',
                                    'solution_is_valid', 'time_is_valid'])):
    """Boundary conditions evaluated for a lead.

    Parameters
    ----------
    hamiltonian : `~scipy.sparse.coo_matrix`
        The Hamiltonian matrix of the boundary conditions evaluated over
        the lead.
    to_slices, from_slices : sequence of `tinyarray.array`
        Each array contains a pair of integers: a slice into ``hamiltonian``
        which we wish to connect *to* (or *from*) the Hamiltonian of the
        central region.
    solution_is_valid : callable
        Function that takes a 1D array of the same size as ``hamiltonian``
        (i.e the solution inside the boundary conditions), and returns
        True if there are no spurious reflections detected.
    time_is_valid : sequence of callable
        Function that takes a float (time) and returns True if the
        boundary conditions will remain valid up till this time.

    Notes
    -----
    ``to_slices`` and ``from_slices`` are used when constructing the
    Hamiltonian for the central system + boundary conditions. Usually The first
    slices will be over the first cell of the lead. Subsequent entries in
    ``from_slices`` can be used, for example, when constructing two "copies" of
    a boundary where the second copy should be affected by what happens in the
    central region, but should not itself have any back-action on the central
    region (see section C of [1] for details).

    [1]: http://arxiv.org/abs/1510.05967
    """


class BoundaryBase(metaclass=abc.ABCMeta):
    """ABC boundary conditions for the time-dependent Schrödinger equation."""

    @abc.abstractmethod
    def __call__(self, lead, args=()):
        """Generate boundary conditions for a single lead.

        These boundary condition matrices must be formatted such that
        they can be coupled to the central system Hamiltonian via
        ``lead.inter_cell_hopping`` in the first ``lead.n_cells``
        orbitals of the matrix corresponding to ``lead``.

        Parameters
        ----------
        lead : `~kwant.system.InfiniteSystem`
            The lead for which to generate boundary conditions.
        args : tuple
            Extra arguments to the Hamiltonian value functions.

        Returns
        -------
        `_EvaluatedBoundary`
        """
        pass


class SimpleBoundary(BoundaryBase):
    """Boundary conditions consisting of N lead unit cells.

    Parameters
    ----------
    num_cells : int, optional
        The number of lead unit cells to add. Mutually exclusive with
        ``max_time``.
    max_time : float, optional
        The maximum time up to which we wish to simulate with these
        boundary conditions. Mutually exclusive with ``num_cells``
    """

    def __init__(self, num_cells=None, max_time=None):
        self.num_cells = self.max_time = None

        if num_cells is not None:
            if num_cells <= 0:
                raise ValueError('Number of unit cells must be positive.')
            self.num_cells = int(num_cells)

        if max_time is not None:
            if max_time <= 0:
                raise ValueError('Maximum time must be positive.')
            self.max_time = float(max_time)

        if self.num_cells is None and self.max_time is None:
            raise TypeError('Either `num_cells` or `max_time` must be provided.')
        elif self.num_cells is not None and self.max_time is not None:
            raise TypeError('`num_cells` and `max_time` are mutually exclusive.')

    def __call__(self, lead, args=()):
        H_cell = lead.cell_hamiltonian(args=((0,) + args), sparse=True)
        V_cell = lead.inter_cell_hopping(args=((0,) + args), sparse=True)
        V_cell = _make_square_matrix(V_cell)
        cell_norbs = H_cell.shape[0]

        # twice the spectral norm of V_cell gives max velocity
        max_velocity = (2 * np.linalg.norm(V_cell.todense(), ord=2))

        max_time = self.max_time or self.num_cells / max_velocity
        # add 1 to prevent off-by-one error due to inter-cell hoppings
        num_cells = (self.num_cells or
                     int(np.ceil(self.max_time * max_velocity)) + 1)

        assert max_time is not None

        H = _make_block_tridiagonal(H_cell, V_cell,
                                    V_cell.conjugate().transpose(),
                                    num_cells)

        def solution_is_valid(lead_psi):
            # With these boundary conditions there is no way to explicitly
            # check if the solution is good.
            return True

        def time_is_valid(next_time):
            # Do not allow us to evolve beyond max_time
            return next_time < max_time

        return EvaluatedBoundary(hamiltonian=H,
                                 to_slices=[ta.array([0, cell_norbs])],
                                 from_slices=[ta.array([0, cell_norbs])],
                                 solution_is_valid=solution_is_valid,
                                 time_is_valid=time_is_valid)


class AbsorbingBoundary(SimpleBoundary):

    def __call__(self, lead, args=()):
        # get the regular boundary conditions by calling superclass
        simple_boundary = super().__call__(lead, args)

        # make diagonal matrix with entries increasing as we go to further into
        # the lead, but with entries constant over a given cell.
        bc_diags = [self._absorb(cell)
                    for cell in range(self.num_cells - self.buffer_cells)]
        bc_diags = [0] * self.buffer_cells + bc_diags  # add buffer cells
        cell_norbs = _get_slice(lead, lead.cell_size).start
        S = sp.kron(sp.diags(bc_diags, offsets=0), sp.eye(cell_norbs))
        H = simple_boundary.hamiltonian - 1j * S

        def solution_is_valid(lead_psi):
            # With these boundary conditions there is no way to explicitly
            # check if the solution is good.
            return True

        def time_is_valid(next_time):
            # apriori there is no "max time" with absorbing boundary conditions
            return True

        return EvaluatedBoundary(hamiltonian=H,
                                 to_slices=simple_boundary.to_slices,
                                 from_slices=simple_boundary.from_slices,
                                 solution_is_valid=solution_is_valid,
                                 time_is_valid=time_is_valid)

    @abc.abstractmethod
    def _absorb(self, cell):
        """Return the absorbing potential in the given cell."""
        pass


class MonomialAbsorbingBoundary(AbsorbingBoundary):
    """Absorbing boundary conditions consisting of N lead unit cells.

    The absorbing region has an imaginary potential applied to it.
    The magnitude of the imaginary potential grows according to
    ``n**degree`` where ``n`` is the index (starting from 0) of the
    lead cell counting from the central region.

    Parameters
    ----------
    num_cells : int
        The number of lead unit cells over which the absorbing potential
        increases.
    strength : float
        The strength of the boundary conditions. Formally
        this is the area underneath the monomial curve.
    degree : int
        The degree of the absorbing monomial.
    buffer_cells : int, optional
        If provided, adds this many lead cells to the start
        of the boundary conditions with no absorbing potential
        applied.
    """

    def __init__(self, num_cells, strength, degree, buffer_cells=0):
        super().__init__(num_cells + buffer_cells)
        self.strength = strength
        if degree < 0:
            raise ValueError('Absorbing boundary conditions cannot be given '
                             'by a monomial of negative degree.')
        if buffer_cells < 0:
            raise ValueError('Cannot add a negative number of buffer cells')
        self.degree = int(degree)
        self.buffer_cells = int(buffer_cells)

    def _absorb(self, x):
        n = self.degree
        ncells = self.num_cells - self.buffer_cells
        return (n + 1) * self.strength * (x**n / ncells**(n + 1))


# TODO: add boundary conditions with a second "copy" that we can use to tell
#       if our solution has any spurious reflection.


################ Internal functions
# TODO: move these to Cython

def _phase_factor_function(phase):
    """Return a function to calculate the phase factor given a phase function.

    i.e. given a function φ(t), returns a function that
    calculates :math:`\exp[iφ(t)]`.
    """
    def phase_factor(time, *args):
        try:
            val = np.asarray(phase(time, *args))
            # if a scalar, `diag` will not work
            if len(val.shape) == 0:
                return cmath.exp(1j * val)
            else:
                return np.diag(np.exp(1j * val))
        except Exception as exc:
            msg = ('Error while evaluating user-supplied '
                   'phase function "{}"'.format(phase.__name__),)
            if exc.args:
                msg += (': ', '; '.join(exc.args))
            raise RuntimeError(''.join(msg)) from exc

    return phase_factor


def _time_dependent_hopping(phase_factor, old_hopping, system_to_lead):
    """Return a time-dependent hopping function.

    If the old hopping is a function, it is always evaluated at time 0.
    """
    if callable(old_hopping):
        if system_to_lead:
            def new_hopping(i, j, t, *args):
                return np.dot(phase_factor(t, *args),
                              old_hopping(i, j, 0., *args))
        else:
            def new_hopping(i, j, t, *args):
                return np.dot(old_hopping(i, j, 0., *args),
                              phase_factor(t, *args).conjugate())
    else:
        if system_to_lead:
            def new_hopping(i, j, t, *args):
                return np.dot(phase_factor(t, *args), old_hopping)
        else:
            def new_hopping(i, j, t, *args):
                return np.dot(old_hopping, phase_factor(t, *args).conjugate())

    # don't forget to explicitly mark the function as time-dependent
    return system.time_dependent(new_hopping)


def _make_block_tridiagonal(D, L, U, n):
    """Make a sparse block-tridiagonal matrix.

    Use ``D`` for the diagonal, ``L`` for the lower
    diagonal, and ``U`` for the upper diagonal. All
    three matrices must be square and have the same shape.

    Parameters
    ----------
    D, L, U : `~scipy.sparse.spmatrix`
        The diagonals, lower diagonals, and upper diagonals.
    n : int
        The number of copies of ``diag`` on the diagonal.

    Returns
    -------
    `~scipy.sparse.lil_matrix`
    """
    if D.shape != L.shape or D.shape != U.shape:
        raise ValueError('Diagonals, lower diagonals and upper diagonals must '
                         ' have the same shape.')
    if D.shape[0] != D.shape[1]:
        raise ValueError('Matrices must be square.')

    sz = D.shape[0]
    M = sp.block_diag([D] * n, format='lil')
    # TODO: convert this to use COO format, for efficiency
    rows = range(0, M.shape[0], sz)
    cols = range(sz, M.shape[0], sz)
    for i, j in zip(rows, cols):
        M[i:(i + sz), j:(j + sz)] = U
        M[j:(j + sz), i:(i + sz)] = L

    return M


def _make_square_matrix(M):
    """Make a square matrix from a rectangular one.

    The rectangular matrix should have shape (n, m)
    with n > m (i.e. upright rectangular). The returned
    matrix will be padded with zeros on the right and
    have shape (n, n).
    """
    if M.shape[0] > M.shape[1]:  # must make matrices square
        n = M.shape[0]
        s = M.shape[0] - M.shape[1]
        M = sp.hstack((M, sp.coo_matrix((n, s), dtype=M.dtype)))
    elif M.shape[0] < M.shape[1]:
        raise ValueError('Matrix must have shape (n, m) with n > m')
    return M


_inf = float('inf')


def _get_slice(syst, site):
    """Return a slice from the first orbital of this site to the next."""
    assert site >= 0 and site < syst.graph.num_nodes
    if not syst.site_ranges:
        raise RuntimeError('Number of orbitals not defined.\n'
                           'Declare the number of orbitals using the '
                           '`norbs` keyword argument when constructing '
                           'the site families (lattices).')
    # Calculate the index of the run that contains the site.
    # The `inf` is needed to avoid the fenceposting problem
    run_idx = bisect.bisect(syst.site_ranges, (site, _inf)) - 1
    # calculate the slice
    first_site, norbs, orb_offset = syst.site_ranges[run_idx]
    orb = orb_offset + (site - first_site) * norbs
    return slice(orb, orb + norbs)
