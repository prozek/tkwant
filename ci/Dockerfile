# Docker image for building and testing tkwant

FROM debian:stable
MAINTAINER Kwant developers <authors@kwant-project.org>

RUN apt-get update && apt-get install -y --no-install-recommends\
    # CI requirements
    apt-transport-https file openssh-client rsync\
    # all the hard non-Python dependencies
    git g++ make patch gfortran libblas-dev liblapack-dev\
    libmumps-scotch-dev pkg-config libfreetype6-dev texlive texlive-latex-extra\
    # all the hard Python dependencies -- use the distribution's
    # repositories so we don't rely on versions that are too new
    python3-all-dev python3-pip python3-setuptools python3-tk python3-matplotlib\
    cython3 python3-numpy python3-scipy python3-mpi4py\
    # needed for rendering jupyter notebooks
    pandoc

# get official kwant
RUN echo "deb http://downloads.kwant-project.org/debian/ jessie-backports main" >> /etc/apt/sources.list &&\
    apt-key adv --keyserver pool.sks-keyservers.net --recv-key C3F147F5980F3535 &&\
    apt-get update &&\
    apt-get install -t jessie-backports python3-tinyarray python3-kwant

# install testing dependencies
RUN pip3 install --upgrade\
    pip pytest pytest-cov pytest-flakes pytest-pep8\
    # and documentation building dependencies
    sphinx sphinx_rtd_theme numpydoc nbsphinx ipykernel jupyter_client

RUN python3 -m ipykernel install --name python3
RUN pip3 install --upgrade cython
