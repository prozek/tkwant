======
tkwant
======
``tkwant`` is an extension to `Kwant <http://kwant-project.org>`_ for dealing
with time-dependent systems.

**Warning**: This is alpha stage software that does not yet have a consistent interface nor a satisfactory test suite and documentation.

Principle of operation
======================
``tkwant`` enables you to simulate time-dependent phenomena with Kwant. You first
define your Kwant system as normal, possibly marking parts of the Hamiltonian
as time-dependent, then you pass this (possibly time-dependent) Kwant system
to a ``tkwant`` solver to calculate time-dependent quantities.

The simplest ``tkwant`` solver will evolve a single-particle wavefunction in
time using the time-dependent Schroedinger equation. ``tkwant`` can handle
both finite systems, and systems with leads attached. In the latter case
you may even specify a scattering state as your initial condition (see
tutorial 1.2 for an example).

``tkwant`` can also calculate observable quantities for time-dependent
systems, which is equivalent to working with the time-dependent non-equilibirum
Green's function (see tutorial 2.1 for an example). A description of the
algorithm can be found `here <https://dx.doi.org/10.1007/s10825-016-0855-9>`_
and `here <https://doi.org/10.1016/j.physrep.2013.09.001>`_.
