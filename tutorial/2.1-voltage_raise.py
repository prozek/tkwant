# -*- coding: utf-8 -*-
# Tutorial 2.1 Thermal Averages
# =============================
#
# To speed up the runtime of this example, run it in parallel with MPI.
# To run on 8 cores, for example, you could use the following command:
#
#   mpirun -n 8 python 2.1-voltage_raise.py
#
# Physics Background
# ------------------
#  Calculating thermal averages of one-body operators evaluated on
#  time-evolved single-particle states.
#
# tkwant features highlighted
# ---------------------------
#  - Use of `tkwant.manybody.Solver`
#  - Use of `tkwant.leads.add_voltage` to add time-dependence to leads.
#  - Use of `tkwant.solve` to solve the time-dependent Schrödinger equation
#    for an open system.
import time as timer
from math import sin, pi
import warnings
import numpy as np
from mpi4py import MPI
import matplotlib.pyplot as plt

with warnings.catch_warnings():
    warnings.simplefilter('ignore')
    import kwant
    import tkwant


def am_master():
    return MPI.COMM_WORLD.rank == 0


def plot_currents(times, tkwant_current, kwant_current):
    plt.plot(times, tkwant_current, lw=3, label='tkwant')
    plt.plot([times[0], times[-1]], [kwant_current] * 2, lw=2, label='kwant')
    plt.legend(loc=4)
    plt.show()


def circle(pos):
    (x, y) = pos
    rsq = x ** 2 + y ** 2
    return rsq < 5 ** 2


def make_system(lat):
    gamma = 1
    syst = kwant.Builder()
    syst[lat.shape(circle, (0, 0))] = 4 * gamma
    syst[lat.neighbors()] = -1 * gamma

    def voltage(site, time, V_static, V_dynamic):
        return 2 * gamma + V_static


    lead = kwant.Builder(kwant.TranslationalSymmetry((-1, 0)))
    lead[lat(0, 0)] = voltage
    lead[lat.neighbors()] = -1 * gamma

    leadr = kwant.Builder(kwant.TranslationalSymmetry((0, -1)))
    leadr[lat(0, 0)] = 2 * gamma
    leadr[lat.neighbors()] = -1 * gamma

    syst.attach_lead(lead)
    syst.attach_lead(leadr)
    return syst


def faraday_flux(time, V_static, V_dynamic):
    omega = 0.1
    t_upper = pi / omega
    if time <= 0:
        return 0
    elif 0 < time < t_upper:
        return V_dynamic * (time - sin(omega * time) / omega) / 2
    else:
        return V_dynamic * (time - t_upper) + V_dynamic * t_upper / 2


def tkwant_calculation(syst, current_operator, tmax, Ef, V_static, V_dynamic):
    ### band structure -- minimum and maximum energy for each lead and mode
    # This will not be necessary once we have the `analyze_bandstructure`
    # function
    assert V_static < Ef
    integration_regions = [(0, 0, (V_static, min(Ef, 4) + V_static)),
                           (1, 0, (0, min(Ef, 4)))]

    ### occupation -- for each lead
    occupations = [tkwant.manybody.fermi_dirac(Ef + V_static, 0),
                   tkwant.manybody.fermi_dirac(Ef, 0)]

    ### boundary conditions for the leads
    boundaries = [tkwant.leads.SimpleBoundary(max_time=tmax) for lead in syst.leads]

    ### Create the solver
    S = tkwant.manybody.Solver(syst, boundaries, occupations, integration_regions,
                               integration_error=1E-5, args=(V_static, V_dynamic))

    ### loop over time, calculating the current as we go
    r = []
    start = timer.clock()
    times = range(tmax)
    for time in times:
        # maybe: S.ensemble_average(operator, time)
        result = S(current_operator, time)
        r.append(result)
        if am_master():
            print('[{:.2f}s] t={}, current is {}'
                  .format(timer.clock() - start, time, r[-1]))

    return times, r


def main():
    lat = kwant.lattice.square(norbs=1)
    syst = make_system(lat)
    # add the "dynamic" part of the voltage
    tkwant.leads.add_voltage(syst, 0, faraday_flux)
    fsyst = syst.finalized()

    if am_master():
        kwant.plot(syst)

    Ef = 1
    tmax = 200

    ### observables
    left_lead_interface = [(lat(0, 0), lat(-1, 0))]
    J = kwant.operator.Current(fsyst, where=left_lead_interface, sum=True)

    # all the voltage applied via the time-dependenece
    V_static = 0
    V_dynamic = 0.5
    times, tkwant_result = tkwant_calculation(fsyst, J, tmax, Ef,
                                              V_static, V_dynamic)

    # all the voltage applied statically
    # even though we are doing a "tkwant calculation", we only calculate
    # the results at t=0.
    V_static = 0.5
    V_dynamic = 0
    _, (kwant_result, *_) = tkwant_calculation(fsyst, J, 1, Ef,
                                               V_static, V_dynamic)

    if am_master():
        times = list(range(tmax))
        plot_currents(times, tkwant_result, kwant_result)


if __name__ == '__main__':
    main()
