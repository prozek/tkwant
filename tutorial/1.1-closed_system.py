# -*- coding: utf-8 -*-
# Tutorial 1.1 Closed Systems
# ===========================
#
# Physics Background
# ------------------
#  Evolving single-particle states forward in time in a closed system,
#  and calculating the expectation value of the radial position operator.
#
# tkwant features highlighted
# ---------------------------
#  - Use of `tkwant.solve` to solve the time-dependent Schrödinger equation
#    for a closed system.

from cmath import exp
from math import cos, sqrt, pi
import numpy as np
from matplotlib import pyplot as plt
import kwant
import tkwant


def make_system(a=1, t=1.0, r=10, r_time_dep=3):
    # Start with an empty tight-binding system and a single square lattice.
    # `a` is the lattice constant (by default set to 1 for simplicity).

    lat = kwant.lattice.square(a, norbs=1)

    syst = kwant.Builder()

    # Define the quantum dot
    def circle(r):
        def _(pos):
            (x, y) = pos
            rsq = x ** 2 + y ** 2
            return rsq < r ** 2
        return _

    # Formally takes the `time` parameter, but does not use it.
    def hopx(site1, site2, time, V=0, B=0):
        # The magnetic field is controlled by the parameter B
        # and oscillates in time
        y = site1.pos[1]
        return -t * exp(-1j * B * y)

    # Decorated to notify tkwant that the function uses the `time` parameter.
    @tkwant.time_dependent
    def potential(site, time, V=0, B=0):
        x, y = site.pos
        return 4 * t + sqrt(x**2 + y**2) * V * (1 - cos(time))

    syst[lat.shape(circle(r), (0, 0))] = 4 * t
    syst[lat.shape(circle(r_time_dep), (0, 0))] = potential
    # hoppings in x-direction
    syst[kwant.builder.HoppingKind((1, 0), lat, lat)] = hopx
    # hoppings in y-directions
    syst[kwant.builder.HoppingKind((0, 1), lat, lat)] = -t

    # It's a closed system, so no leads
    return syst


def main():
    syst = make_system().finalized()

    V=1.0
    B=1.0

    H = syst.hamiltonian_submatrix(args=(0, V, B))
    evals, evecs = np.linalg.eigh(H)

    # create an observable for calculating the average radius of a wavefunction
    def radius(site, *args):
        x, y = site.pos
        return sqrt(x**2 + y**2)

    R = kwant.operator.Density(syst, radius, sum=True).bind()  # pre-bind for speed

    # create a time-dependent wavefunction that starts in the ground state
    ground_state = tkwant.solve(syst, args=(V, B),
                                energy=evals[0], psi_init=evecs[:, 0])

    # evolve forward in time, calculating the average radius of the wavefunction
    times = np.arange(0, 10*pi, 0.1)
    rad = [R(ground_state(time)) for time in times]

    plt.plot(times, rad)
    plt.show()


if __name__ == '__main__':
    main()
