# -*- coding: utf-8 -*-
# Tutorial 1.4 Alternative Boundary Conditions
# ============================================
#
# tkwant features highlighted
# ---------------------------
#  - Directly using the `tkwant.onebody.Solver` interface (and providing
#    explicit boundary conditions), instead of the simpler `tkwant.solve`.
#  - Selecting alternative boundary conditions to improve performance

import time as timer
from math import sin, pi
import numpy as np
from matplotlib import pyplot as plt

import kwant
import tkwant


def make_system(a=1, t=1.0, r=10):
    # Start with an empty tight-binding system and a single square lattice.
    # `a` is the lattice constant (by default set to 1 for simplicity).

    lat = kwant.lattice.square(a, norbs=1)

    syst = kwant.Builder()

    # Define the quantum dot
    def circle(pos):
        (x, y) = pos
        rsq = x ** 2 + y ** 2
        return rsq < r ** 2

    syst[lat.shape(circle, (0, 0))] = 4 * t
    syst[lat.neighbors()] = -t

    lead = kwant.Builder(kwant.TranslationalSymmetry((-1, 0)))
    lead[(lat(0, j) for j in range(-r//2 + 2, r//2 - 1))] = 4 * t
    lead[lat.neighbors()] = -t

    syst.attach_lead(lead)
    syst.attach_lead(lead.reversed())

    return syst


# for adding a time-dependent voltage on top of the leads
def faraday_flux(time):
    return 0.1 * (time - 10 * sin(0.1 * time)) / 2


def evolve(times, psi, operator, title):
    ops = []
    for time in times:
        expectation = operator(psi(time), args=(time,))
        ops.append(expectation)
    return ops


def main():
    syst = make_system()

    kwant.plot(syst)

    # add a time-dependent voltage to lead 0 -- this is implemented
    # by adding sites to the system at the interface with the lead and
    # multiplying the hoppings to these sites by exp(-1j * faraday_flux(time))
    extra_sites = tkwant.leads.add_voltage(syst, 0, faraday_flux)
    lead_syst_hoppings = [(s, site) for site in extra_sites
                            for s in syst.neighbors(site)
                            if s not in extra_sites]

    syst = syst.finalized()

    # create an observable for calculating the current flowing from the left lead
    J = kwant.operator.Current(syst, where=lead_syst_hoppings, sum=True)

    energy = 1.
    tmax = 200 * pi

    # create initial scattering state
    scattering_states = kwant.wave_function(syst, energy=1., args=(0,))
    psi_st = scattering_states(0)[0]


    # boundary conditions typically used by `tkwant.solve`
    simple_boundaries = [tkwant.leads.SimpleBoundary(max_time=tmax)
                         for l in syst.leads]

    # boundary conditions with an absorbing potential that increases
    # according to x**n where `x` is the distance into the lead
    absorbing_boundaries = [tkwant.leads.MonomialAbsorbingBoundary(
                                num_cells=100, strength=10, degree=6)
                            for l in syst.leads]


    # create time-dependent wavefunctions that starts in a scattering state
    # originating from the left lead, using two different types of boundary
    # conditions
    solver = tkwant.onebody.solvers.default
    psi_simple = tkwant.solve(syst, boundaries=simple_boundaries,
                              psi_init=psi_st, energy=energy,
                              solver_type=solver)
    psi_alt = tkwant.solve(syst, boundaries=absorbing_boundaries,
                           psi_init=psi_st, energy=energy,
                           solver_type=solver)


    # evolve forward in time, calculating the current
    times = np.arange(0, tmax)

    start = timer.clock()
    current_simple = evolve(times, psi_simple, J, 'Simple boundary conditions')
    stop = timer.clock()
    print('Simple Boundary conditions elapsed time: {:.2f}s'.format(stop - start))

    start = timer.clock()
    current_alt = evolve(times, psi_alt, J, 'Absorbing boundary conditions')
    stop = timer.clock()
    print('Absorbing Boundary conditions elapsed time: {:.2f}s'.format(stop - start))

    plt.plot(times, current_simple, lw=2, label='SimpleBoundary')
    plt.plot(times, current_alt, '--', lw=2, label='MonomialAbsorbingBoundary')
    plt.legend(loc=4)
    plt.show()


if __name__ == '__main__':
    main()
