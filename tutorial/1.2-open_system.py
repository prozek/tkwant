# -*- coding: utf-8 -*-
# Tutorial 1.2 Open Systems
# =========================
#
# Physics Background
# ------------------
#  Evolving single-particle states forward in time in an open system,
#  and calculating the expectation value of the current.
#
# tkwant features highlighted
# ---------------------------
#  - Use of `tkwant.leads.add_voltage` to add time-dependence to leads.
#  - Use of `tkwant.solve` to solve the time-dependent Schrödinger equation
#    for an open system.

import time
from math import sin, pi
import numpy as np
from matplotlib import pyplot as plt
import kwant
import tkwant


def make_system(a=1, t=1.0, r=10):
    # Start with an empty tight-binding system and a single square lattice.
    # `a` is the lattice constant (by default set to 1 for simplicity).

    lat = kwant.lattice.square(a, norbs=1)

    syst = kwant.Builder()

    # Define the quantum dot
    def circle(pos):
        (x, y) = pos
        rsq = x ** 2 + y ** 2
        return rsq < r ** 2

    syst[lat.shape(circle, (0, 0))] = 4 * t
    syst[lat.neighbors()] = -t

    lead = kwant.Builder(kwant.TranslationalSymmetry((-1, 0)))
    lead[(lat(0, j) for j in range(-r//2 + 1, r//2))] = 4 * t
    lead[lat.neighbors()] = -t

    syst.attach_lead(lead)
    syst.attach_lead(lead.reversed())

    return syst


# time-dependent voltage for leads
def faraday_flux(time):
    return 0.1 * (time - 10 * sin(0.1 * time)) / 2


def main():
    syst = make_system()

    # add a time-dependent voltage to lead 0 -- this is implemented
    # by adding sites to the system at the interface with the lead and
    # multiplying the hoppings to these sites by exp(-1j * faraday_flux(time))
    extra_sites = tkwant.leads.add_voltage(syst, 0, faraday_flux)
    lead_syst_hoppings = [(s, site) for site in extra_sites
                            for s in syst.neighbors(site)
                            if s not in extra_sites]

    syst = syst.finalized()

    # create an observable for calculating the current flowing from the left lead
    J = kwant.operator.Current(syst, where=lead_syst_hoppings, sum=True)

    # create a time-dependent wavefunction that starts in a scattering state
    # originating from the left lead
    scattering_states = kwant.wave_function(syst, energy=1., args=(0,))
    psi_st = scattering_states(0)[0]
    psi = tkwant.solve(syst, tmax=100 * pi, psi_init=psi_st, energy=1.)

    # evolve forward in time, calculating the current
    times = np.arange(0, 100 * pi)
    current = [J(psi(time), args=(time,)) for time in times]

    plt.plot(times, current)
    plt.show()


if __name__ == '__main__':
    main()
