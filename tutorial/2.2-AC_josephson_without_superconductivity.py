# -*- coding: utf-8 -*-
# Tutorial 2.2 AC Josephson effect without superconductivity
# ==========================================================
#
# To speed up the runtime of this example, run it in parallel with MPI.
# To run on 8 cores, for example, you could use the following command:
#
#   mpirun -n 8 python 2.2-AC_josephson_without_superconductivity.py
#
# Physics Background
# ------------------
# Applying voltage pulses to Fabry-Perot cavities to illustrate dynamic
# control of interference[1].
#
# [1]: http://www.nature.com/ncomms/2015/150313/ncomms7524/full/ncomms7524.html

import time as timer
import functools as ft
from math import sin, pi
import warnings
import numpy as np
from mpi4py import MPI
import matplotlib.pyplot as plt

with warnings.catch_warnings():
    warnings.simplefilter('ignore')
    import kwant
    import tkwant


def am_master():
    return MPI.COMM_WORLD.rank == 0


def plot_currents(times, r):
    plt.plot(times, r, lw=3, label='tkwant')
    plt.legend(loc=4)
    plt.show()


def barrier(site, time, V_b, V_bias, tau):
    return 2 + V_b


# Raise voltage over a time `tau`
def faraday_flux(time, V_b, V_bias, tau):
    omega = pi / tau
    if time <= 0:
        return 0
    elif 0 < time < tau:
        return V_bias * (time - sin(omega * time) / omega) / 2
    else:
        return V_bias * (time - tau) + V_bias * tau / 2


def make_system(L):
    lat = kwant.lattice.chain(norbs=1)
    syst = kwant.Builder()
    syst[map(lat, range(L + 2))] = 2
    syst[lat.neighbors()] = -1

    syst[lat(0)] = syst[lat(L)] = barrier

    lead = kwant.Builder(kwant.TranslationalSymmetry((-1,)))
    lead[lat(0)] = 2
    lead[lat.neighbors()] = -1

    syst.attach_lead(lead)
    syst.attach_lead(lead.reversed())
    return lat, syst


def main():
    L = 400
    Ef = 1.
    tmax = 500

    lat, syst = make_system(L)
    # add the "dynamic" part of the voltage
    tkwant.leads.add_voltage(syst, 0, faraday_flux)
    fsyst = syst.finalized()

    if am_master():
        kwant.plot(syst)

    ### integration_regions -- minimum and maximum energy for each lead and mode
    integration_regions = [((1, 0), 0, (0, Ef))]

    ### occupation for each lead
    occupations = [tkwant.manybody.fermi_dirac(Ef, 0)] * 2

    ### boundary conditions for the leads
    boundaries = [tkwant.leads.MonomialAbsorbingBoundary(
                    num_cells=100, strength=20, degree=6)
                  for lead in syst.leads]

    V_b = 0.5
    V_bias = 0.2
    tau = 10

    ### Create the solver
    integrator = ft.partial(tkwant.integration.adaptive_gauss_kronrod, limit=-1)
    S = tkwant.manybody.Solver(fsyst, boundaries, occupations, integration_regions,
                               integrator_factory=integrator,
                               integration_error=1E-5, args=(V_b, V_bias, tau))

    ### observables
    left_lead_interface = [(lat(L + 1), lat(L))]
    J = kwant.operator.Current(fsyst, where=left_lead_interface, sum=True)

    ### loop over time, calculating the current as we go
    r = []
    start = timer.clock()
    times = range(tmax)
    for time in times:
        # maybe: S.ensemble_average(operator, time)
        result = S(J, time)
        r.append(result)
        if am_master():
            print('[{:.2f}s] t={}, current is {}'
                  .format(timer.clock() - start, time, r[-1]))

    if am_master():
        plot_currents(times, r)


if __name__ == '__main__':
    main()
