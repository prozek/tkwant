:mod:`tkwant` -- Top level package
==================================

.. module:: tkwant

For convenience, short names are provided for a few widely used objects from
the sub-packages.
Otherwise, this package has only very limited functionality of its own.

Generic functionality
---------------------
The version of tkwant is available under the name ``__version__``.

.. autosummary::
   :toctree: generated/

   TkwantDeprecationWarning

From `tkwant.system`
--------------------
.. currentmodule:: tkwant.system
.. autosummary::
   :toctree: generated/

    time_dependent

From `tkwant.onebody`
---------------------
.. currentmodule:: tkwant.onebody
.. autosummary::
   :toctree: generated/

    solve
    restart
