:mod:`tkwant.onebody.kernel` -- Evaluating the right hand side of the time-dependent Schrödinger equation
=========================================================================================================
This module is mainly for internal use by `tkwant.onebody.solver`.

Kernels
-------
.. module:: tkwant.onebody.kernels
.. autosummary::
    :toctree: generated

    Scipy
    SparseBlas
