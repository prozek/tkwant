:mod:`tkwant.integrate` -- Adaptive, asynchronous numeric integration
=====================================================================

.. module:: tkwant.integration

Integrators
-----------
These functions are meant to be used directly.

.. autosummary::
    :toctree: generated

    adaptive_simpson
    adaptive_gauss_kronrod
    fixed_gauss_kronrod


Low-level interface
-------------------
These functions are the building blocks on which the above integrators are
built.

.. autosummary::
    :toctree: generated

    adaptive_integration
    Interval
    SimpsonInterval
    GK15Interval
