:mod:`tkwant.onebody` -- Solving the one-body time-dependent Schrödinger equation
=================================================================================

Solvers
-------
.. module:: tkwant.onebody.solvers
.. autosummary::
    :toctree: generated

    Scipy
    Boost
